const appParams = {
  // serverBaseAddress: 'http://bistopanj.com/',
  serverBaseAddress: '/',
  minOtpLen: 3,
  userIdCookieExpDays: 180,
  defaultToastDuration: 5000, // miliseconds
  googleAnalyticsTrackingId: 'UA-150353421-1',
  cookieKeys: {
    token: 'token',
    userId: 'userId'
  }
};

const langs = {
  en: { title: 'English', dir: 'ltr' },
  fr: { title: 'Français', dir: 'ltr' },
  fa: { title: 'فارسی', dir: 'rtl' },
  ar: { title: 'العربية', dir: 'rtl' }
};

const sampleResults = {
  fa: '5de213aef73ca900043d41a1',
  ar: '5de2141df73ca900043d41a4',
  en: '5de213e6f73ca900043d41a2',
  fr: '5de2140ff73ca900043d41a3'
};

export default { appParams, langs, sampleResults };
export { appParams, langs, sampleResults };

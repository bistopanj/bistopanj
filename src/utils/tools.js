import jalaali from 'jalaali-js';
import queryString from 'query-string';

const romanDigitLookup = {};
romanDigitLookup[String.fromCharCode(1776)] = '0';
romanDigitLookup[String.fromCharCode(1777)] = '1';
romanDigitLookup[String.fromCharCode(1778)] = '2';
romanDigitLookup[String.fromCharCode(1779)] = '3';
romanDigitLookup[String.fromCharCode(1780)] = '4';
romanDigitLookup[String.fromCharCode(1781)] = '5';
romanDigitLookup[String.fromCharCode(1782)] = '6';
romanDigitLookup[String.fromCharCode(1783)] = '7';
romanDigitLookup[String.fromCharCode(1784)] = '8';
romanDigitLookup[String.fromCharCode(1785)] = '9';
romanDigitLookup[String.fromCharCode(1632)] = '0';
romanDigitLookup[String.fromCharCode(1633)] = '1';
romanDigitLookup[String.fromCharCode(1634)] = '2';
romanDigitLookup[String.fromCharCode(1635)] = '3';
romanDigitLookup[String.fromCharCode(1636)] = '4';
romanDigitLookup[String.fromCharCode(1637)] = '5';
romanDigitLookup[String.fromCharCode(1638)] = '6';
romanDigitLookup[String.fromCharCode(1639)] = '7';
romanDigitLookup[String.fromCharCode(1640)] = '8';
romanDigitLookup[String.fromCharCode(1641)] = '9';

const persianDigitLookup = {
  0: String.fromCharCode(1776),
  1: String.fromCharCode(1777),
  2: String.fromCharCode(1778),
  3: String.fromCharCode(1779),
  4: String.fromCharCode(1780),
  5: String.fromCharCode(1781),
  6: String.fromCharCode(1782),
  7: String.fromCharCode(1783),
  8: String.fromCharCode(1784),
  9: String.fromCharCode(1785)
};

const convertNumeralsToRoman = text => {
  if (typeof text !== 'string') throw new Error('The input should be a string');
  const converted = text.split('').reduce((acc, c) => {
    const convertedChar = c in romanDigitLookup ? romanDigitLookup[c] : c;
    const newAcc = `${acc}${convertedChar}`;
    return newAcc;
  }, '');
  return converted;
};

const convertNumeralsToPersian = text => {
  if (typeof text !== 'string') throw new Error('The input should be a string');
  const converted = text.split('').reduce((acc, c) => {
    const convertedChar = c in persianDigitLookup ? persianDigitLookup[c] : c;
    const newAcc = `${acc}${convertedChar}`;
    return newAcc;
  }, '');
  return converted;
};

const buildSrcSet = set =>
  Object.keys(set)
    .map(key => `${set[key]} ${key}`)
    .join(',');

const isPhoneNumber = text => {
  const pattern = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g;
  return pattern.test(text);
};

const parseMobileNumber = string => {
  try {
    const romanText = convertNumeralsToRoman(string);
    if (!isPhoneNumber(romanText)) return null;

    const normalizedText = romanText.replace(/\D/g, '').replace(/^0+/, '');
    // const normalizedText = romanText.replace(/\D/g, '');
    // const normalizedText = romanText.replace(/(?<!^)[^0-9]|^[^0-9+]/g, '');
    // const nakedText = normalizedText.replace(/^[0+]/, '');
    const minLen = /^98/g.test(normalizedText) ? 12 : 10;

    if (normalizedText.length >= minLen) return { standardFormat: romanText };

    return null;
  } catch (error) {
    return null;
  }
};

const polishNumber = (string, lang = 'fa') => {
  const polishMethod = {
    fa: convertNumeralsToPersian,
    ar: convertNumeralsToPersian
  };

  return lang in polishMethod ? polishMethod[lang](string) : string;
};

const buildQueryString = (query = {}) => {
  const str = Object.keys(query)
    .filter(k => query[k])
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(query[k])}`)
    .join('&');
  return str;
};

const parseQueryString = str => queryString.parse(str);

const getIranTime = date => {
  const theDate = new Date(date);
  const timeStr = `${theDate.getHours()}:${theDate.getMinutes()}:${theDate.getSeconds()}`;
  return convertNumeralsToPersian(timeStr);
};

const getIranDate = date => {
  const theDate = new Date(date);
  const { jy, jm, jd } = jalaali.toJalaali(theDate);
  const persianDateStr = `${jy}/${jm}/${jd}`;
  return convertNumeralsToPersian(persianDateStr);
};

const getLocalTime = (date, lang = 'fa') => {
  if (lang === 'fa') return getIranTime(date);
  const theDate = new Date(date);
  const timeStr = theDate.toLocaleTimeString(lang);
  return polishNumber(timeStr, lang);
};

const getLocalDate = (date, lang = 'fa') => {
  if (lang === 'fa') return getIranDate(date);
  const theDate = new Date(date);
  const dateStr = theDate.toLocaleDateString(lang);
  return polishNumber(dateStr, lang);
};

const getMobileOS = () => {
  const userAgent =
    navigator.userAgent || navigator.vendor || window.DOMPointReadOnly;

  if (/windows phone/i.test(userAgent)) return 'Windows Phone';
  if (/android/i.test(userAgent)) return 'Android';
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) return 'iOS';
  return 'unknown';
};

export {
  buildSrcSet,
  parseMobileNumber,
  convertNumeralsToRoman,
  convertNumeralsToPersian,
  buildQueryString,
  parseQueryString,
  getIranTime,
  getIranDate,
  polishNumber,
  getLocalDate,
  getLocalTime,
  getMobileOS
};

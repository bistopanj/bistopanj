import labLogo from '../assets/lab_logo.png';
import labLogo2x from '../assets/lab_logo@2x.png';
import labLogo3x from '../assets/lab_logo@3x.png';
import newProductLogo from '../assets/new_product_logo.png';
import newProductLogo2x from '../assets/new_product_logo@2x.png';
import newProductLogo3x from '../assets/new_product_logo@3x.png';

const lab = {
  name: 'company.home.lab',
  description: 'company.home.lab-desc',
  for: 'company.home.med-labs',
  logo: {
    src: labLogo,
    set: {
      '2x': labLogo2x,
      '3x': labLogo3x
    },
    alt: 'lab'
  },
  href: '/lab',
  to: '/lab',
  key: 'lab',
};
const products = [ lab ];



const productsWithSuggestion = [
  lab,
  {
    name: 'company.home.your-need',
    description: 'company.home.your-need-desc',
    for: 'company.home.your-work',
    logo: {
      src: newProductLogo,
      set: {
        '2x': newProductLogo2x,
        '3x': newProductLogo3x
      },
      alt: 'question-mark'
    },
    href: 'mailto:hi@bistopanj.com?Subject=Product Suggestion',
    key: 'suggest',
    buttonText: 'company.home.suggest'
  }
];

export default products;
export { products, productsWithSuggestion }

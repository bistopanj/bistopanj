const dashboardMenuItems = [
  { title: 'lab.dashboard.home', to: '/lab/dashboard' },
  { title: 'lab.dashboard.mobile-apps', to: '/lab/dashboard/apps' },
  { title: 'lab.dashboard.manage-personnel', to: '/lab/dashboard/lab-personnel' },
  { title: 'lab.dashboard.results', to: '/lab/dashboard/test-results' },
  { title: 'lab.dashboard.settings', to: '/lab/dashboard/settings' },
  { title: 'lab.dashboard.logout', to: '/lab/logout' },
];

export default dashboardMenuItems;

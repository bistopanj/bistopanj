const defaultMenuItems = [
  { title: 'nav.lab', to: '/lab', key: 'lab' },
  { title: 'nav.contacts', to: '/contacts', key: 'contacts' },
];

export default defaultMenuItems;

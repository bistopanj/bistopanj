const appDownloadLinks = {
  cafebazaar: 'https://install.appcenter.ms/orgs/bistopanj/apps/lab',
  googleplay: 'https://install.appcenter.ms/orgs/bistopanj/apps/lab',
  ios: 'https://install.appcenter.ms/orgs/bistopanj/apps/lab-1'
};

export default appDownloadLinks;

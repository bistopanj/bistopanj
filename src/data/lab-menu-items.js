const labMenuItems = {
  loggedOut: [
    { title: 'lab.nav.set-up', to: '/lab/register', key: 'register' },
    { title: 'lab.nav.sign-in', to: '/lab/dashboard', key: 'enter' },
  ],
  loggedIn: [
    { title: 'lab.nav.my-lab', to: '/lab/dashboard', key: 'dashboard' },
    { title: 'lab.nav.log-out', to: '/lab/logout', key: 'logout' },
  ],
};

export default labMenuItems;

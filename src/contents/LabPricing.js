const plans = [
  {
    title: 'lab.landing.pricing-eval-title',
    description: 'lab.landing.pricing-eval-desc',
    sendCredit: 'general.twenty',
    price: 'general.zero',
    usersPerLab: 'general.five',
    buttonText: 'lab.landing.pricing-eval-start',
    buttonHref: '/lab/register?plan=free',
    belowButton: 'lab.landing.pricing-eval-more-info',
  },
  {
    title: 'lab.landing.pricing-prem-title',
    description: 'lab.landing.pricing-prem-desc',
    sendCredit: 'general.unlimited',
    price: 'lab.landing.price',
    usersPerLab: 'general.five',
    buttonText: 'lab.landing.pricing-prem-start',
    buttonHref: '/lab/register?plan=standard',
    belowButton: 'lab.landing.pricing-prem-more-info',
  }
];

export default { plans };
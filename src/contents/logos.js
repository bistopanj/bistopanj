import { buildSrcSet } from '../utils/tools';

import bistopanjLogo from '../assets/bistopanj_logo.png';
import bistopanjLogo2x from '../assets/bistopanj_logo@2x.png';
import bistopanjLogo3x from '../assets/bistopanj_logo@3x.png';

import labLogo from '../assets/lab_logo.png';
import labLogo2x from '../assets/lab_logo@2x.png';
import labLogo3x from '../assets/lab_logo@3x.png';

const labLogoSet = {
  '2x': labLogo2x,
  '3x': labLogo3x
};

const lab = {
  src: labLogo,
  srcSet: buildSrcSet(labLogoSet),
  alt: 'لب برای آزمایشگاه‌های پزشکی'
};

const bistopanjLogoSet = {
  '2x': bistopanjLogo2x,
  '3x': bistopanjLogo3x,
};

const bistopanj = {
  src: bistopanjLogo,
  srcSet: buildSrcSet(bistopanjLogoSet),
  alt: 'ابزارهای اینترنتی برای کسب‌و‌کارها'
};

export default { lab, bistopanj };
export { lab, bistopanj };

/*
    This is the content for the ListJumbo at LabValueJumbo.
    It contains the main differentiation of Lab for the patients of the laboratories.
*/

const title = 'بدون درد سر برای مشتری‌ها';
const subtitle = 'به ساده‌ترین شکل ممکن نتیجه را برای بیماران‌تان بفرستید.';
const items = [
  'احتیاج به نصب کردن هیچ نرم‌افزار توسط بیمار نیست.',
  'احتیاج به وارد کردن هیچ رمز، نام کاربری یا کد پی‌گیری نیست.',
  'بیمار می‌تواند نتیجه‌ی آزمایش را به عنوان یک عکس در گوشی‌اش ذخیره کند.',
  'پیامک برای شماره‌ی موبایلی فرستاده می‌شود که بیمار در زمان آزمایش ارائه کرده.'
];

export default { title, subtitle, items };

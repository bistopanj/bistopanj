/*
    This is the content for the CompanyWay Jumbo in the Home page.
    It shows the most important methodological approaches of bistopanj.
*/

const title = 'company.home.diff-title';
const subtitle = 'company.home.diff-subtitle';
const items = [
  { logo: 'plug', title: 'company.home.simple', body: 'company.home.simple-desc' },
  { logo: 'shipping-fast', title: 'company.home.fast', body: 'company.home.fast-desc' }
];

export default { title, subtitle, items };
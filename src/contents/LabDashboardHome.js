const descriptionItems = [
  {
    key: 'dashboard-home-1',
    text: 'lab.dashboard.mobile-apps-desc',
    title: 'lab.dashboard.mobile-apps',
    logo: 'mobile',
    to: '/lab/dashboard/apps',
  },
  {
    key: 'dashboard-home-2',
    text: 'lab.dashboard.manage-personnel-desc',
    title: 'lab.dashboard.manage-personnel',
    logo: 'users',
    to: '/lab/dashboard/lab-personnel',
  },
  {
    key: 'dashboard-home-3',
    text: 'lab.dashboard.results-desc',
    title: 'lab.dashboard.results',
    logo: 'receipt',
    to: '/lab/dashboard/test-results',
  },
  {
    key: 'dashboard-home-4',
    text: 'lab.dashboard.settings-desc',
    title: 'lab.dashboard.settings', 
    logo: 'config',
    to: '/lab/dashboard/settings',
  }
];

export default descriptionItems;

/*
    This is the content for the LabOwnerValue Jumbo in the Lab page.
    It contains the main differentiation of Lab for laboratories.
*/

const title = 'lab.landing.owner-value-title';
const subtitle = 'lab.landing.owner-value-subtitle';
const items = [
  {
    logo: 'mobile',
    title: 'lab.landing.just-mobile-number',
    body: 'lab.landing.just-mobile-number-desc',
  },
  {
    logo: 'camera',
    title: 'lab.landing.just-take-picture',
    body: 'lab.landing.just-take-picture-desc',
  },
  {
    logo: 'puzzle',
    title: 'lab.landing.no-other-change',
    body: 'lab.landing.no-other-change-desc',
  },
  {
    logo: 'money',
    title: 'lab.landing.no-extra-cost',
    body: 'lab.landing.no-extra-cost-desc',
  }
];

export default { title, subtitle, items };

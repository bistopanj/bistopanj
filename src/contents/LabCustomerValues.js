/*
    This is the content for the LabCustomerValues Jumbo in the Lab page.
    It contains the main differentiation of Lab for the patients of the laboratories.
*/

const title = 'lab.landing.customer-value-title';
const subtitle = 'lab.landing.customer-value-subtitle';
const items = [
  {
    logo: 'envelope-open',
    title: 'lab.landing.one-click',
    body: 'lab.landing.one-click-desc',
  },
  {
    logo: 'couch',
    title: 'lab.landing.not-complex',
    body: 'lab.landing.not-complex-desc',
  },
  {
    logo: 'text-message',
    title: 'lab.landing.easy-delivery',
    body: 'lab.landing.easy-delivery-desc',
  },
  {
    logo: 'image',
    title: 'lab.landing.easy-save',
    body: 'lab.landing.easy-save-desc',
  }
];

export default { title, subtitle, items };

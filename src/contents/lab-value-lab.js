/*
    This is the content for the SampleResultJumbo at LabValueJumbo.
    It contains the main values of Lab for the laboratoris.
*/

const title = 'احتیاج به هیچ تغییری در پروسه‌‌های کاری‌تان نیست';
const items = [
  'فقط کافی است از بیمارانی که می‌خواهند نتیجه‌ی آزمایش را الکترونیکی دریافت کنند شماره موبایل‌شان را بگیرید.',
  'وقتی نتیجه‌ی آزمایش آماده شد، از برگه‌ی نتیجه عکس گرفته می‌شود.',
  'فرقی نمی‌کند در حال حاضر از چه سیستم‌هایی استفاده می‌کنید.',
  'تنها چیزی که احتیاج است یک گوشی هوشمند است و اتصال به اینترنت.'
];

export { title, items };

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NotFound from './NotFound';
import Lab from './lab/Lab';
import LabHeadTags from '../components/lab/LabHeadTags';
import LabUserAgreement from './lab/LabUserAgreement';
import LabRegister from './lab/LabRegister';
import LabRegisterLimited from './lab/LabRegisterLimited';
import LabLogin from './lab/LabLogin';
import LabWhitelist from './lab/LabWhitelist';
import LabCreate from './lab/LabCreate';
import LabChoose from './lab/LabChoose';
import LabUpdate from './lab/LabUpdate';
import LabDashboard from './lab/LabDashboard';
import LabLogout from './lab/LabLogout';
import LabResult from './lab/LabResult';
import LabMobileAppRouter from './lab/LabMobileAppRouter';
import PrivateRoute from '../components/PrivateRoute';

const LabRouter = () => (
  <>
    <LabHeadTags />
    <Switch>
      <Route exact path="/lab/" component={Lab} />
      <Route exact path="/lab/user-agreement" component={LabUserAgreement} />
      <Route exact path="/lab/register" component={LabRegister} />
      <Route exact path="/lab/register-limited" component={LabRegisterLimited} />
      <Route exact path="/lab/login" component={LabLogin} />
      <PrivateRoute exact path="/lab/login" component={LabLogin} />
      <Route exact path="/lab/logout" component={LabLogout} />
      <PrivateRoute exact path="/lab/whitelist" component={LabWhitelist} />
      <PrivateRoute exact path="/lab/update" component={LabUpdate} />
      <PrivateRoute
        exact
        path="/lab/create"
        component={LabCreate}
        requires={['token', 'labs']}
      />
      <PrivateRoute
        exact
        path="/lab/choose"
        component={LabChoose}
        requires={['token', 'labs']}
      />
      <PrivateRoute
        path="/lab/dashboard"
        component={LabDashboard}
        requires={['token', 'labs', 'selectedLab']}
      />
      <Route exact path="/lab/result/:resultId" component={LabResult} />
      <Route path="/lab/mobile-app" component={LabMobileAppRouter} />
      <Route component={NotFound} />
    </Switch>
  </>
);

export default LabRouter;

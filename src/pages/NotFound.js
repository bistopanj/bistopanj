import React from 'react';
import { Helmet } from 'react-helmet';
import TextPage from '../components/TextPage';
import { useAppContext } from '../contexts';

const NotFound = () => {
  const { t } = useAppContext();
  
  return (
    <TextPage>
      <Helmet>
        <title> {t('not-found')} </title>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Helmet>
      <p>{t('not-found')}</p>
    </TextPage>
  );
};

export default NotFound;

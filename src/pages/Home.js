import React from 'react';
import { Helmet } from 'react-helmet';
import '../App.css';
import CompanyValue from '../components/company/CompanyValue';
import CompanyProducts from '../components/company/CompanyProducts';
import CompanyWay from '../components/company/CompanyWay';

const Home = () => (
  <main role="main">
    <Helmet>
      <link rel="shortcut icon" href="/favicon.ico" />
    </Helmet>
    <CompanyValue />
    <CompanyProducts />
    <CompanyWay />
  </main>
);

export default Home;

import React from 'react';
import { Helmet } from 'react-helmet';
import TextPage from '../components/TextPage';
import { useAppContext } from '../contexts'; 

const Contacts = () => {
  const { t } = useAppContext();

  return (
    <TextPage>
      <Helmet>
        <title> {t('nav.contacts')} </title>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Helmet>
      <h4>
        {t('nav.contacts')}
      </h4>
      <p>
        {t('contacts.use-this-mail')}
      </p>
      <a href="mailto:hi@bistopanj.com" target="_top">hi@bistopanj.com</a>
    </TextPage>
  );
};

export default Contacts;

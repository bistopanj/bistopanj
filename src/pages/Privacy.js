import React from 'react';
import { Helmet } from 'react-helmet';
import TextPage from '../components/TextPage';
import { useAppContext } from '../contexts';


const Privacy = () => {
  const { t } = useAppContext();
  return (
    <TextPage>
      <Helmet>
        <title> {t('tags.site-name-default')}{' | '}{t('privacy.title')} </title>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Helmet>
      <h4>{t('privacy.title')}</h4>
      <p>{t('privacy.p1')}</p>
      <p>{t('privacy.p2')}</p>
      <p>{t('privacy.p3')}</p>
      <ol>
        <li>{t('privacy.ol1-1')}</li>
        <li>{t('privacy.ol1-2')}</li>
        <li>{t('privacy.ol1-3')}</li>
        <li>{t('privacy.ol1-4')}</li>
      </ol>
      <h5>{t('privacy.h1')}</h5>
      <p>{t('privacy.p4')}</p>
      <ol>
        <li>{t('privacy.ol2-1')}</li>
        <li>{t('privacy.ol2-2')}</li>
        <li>{t('privacy.ol2-3')}</li>
      </ol>
      <h5>{t('privacy.h2')}</h5>
      <p>{t('privacy.p5')}</p>
      <ol>
        <li>{t('privacy.ol3-1')}</li>
        <li>{t('privacy.ol3-2')}</li>
      </ol>
      <p>{t('privacy.p6')}</p>
      <ol>
        <li>{t('privacy.ol4-1')}</li>
        <li>{t('privacy.ol4-2')}</li>
        <li>{t('privacy.ol4-3')}</li>
      </ol>
      <p>{t('privacy.p7')}</p>
      <h5>{t('privacy.h3')}</h5>
      <p>{t('privacy.p8')}</p>
      <p>{t('privacy.p9')}</p>
      <p>{t('privacy.p10')}</p>
      <h5>{t('privacy.h4')}</h5>
      <p>{t('privacy.p11')}</p>
      <p>{t('privacy.p12')}</p>
      <h5>{t('privacy.h5')}</h5>
      <p>{t('privacy.p13')}</p>
      <p>{t('privacy.p14')}</p>
      <p>{t('privacy.p15')}</p>
      <h5>{t('privacy.h6')}</h5>
      <p>{t('privacy.p16')}</p>
      <h5>{t('privacy.h7')}</h5>
      <p>{t('privacy.p17')}</p>
      <p>{t('privacy.p18')}</p>
      <p>{t('privacy.p19')}</p>
      <h5>{t('privacy.h8')}</h5>
      <p>{t('privacy.p20')}</p>
    </TextPage>
  );
};

export default Privacy;
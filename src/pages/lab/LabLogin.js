import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';

import LabLoginForm from '../../components/forms/LabLoginForm';
import FullPageContainer from '../../components/FullPageContainer';
import { AuthContext, useAppContext } from '../../contexts';


const FooterRow = () => {
  const { t } = useAppContext();
  return (
    <div style={{ marginTop: 5 }}>
      <Link to="/lab" className="footer-link" >
        {t('lab.register.return-to-lab')}
      </Link>
    </div>
  );
};


class LabLogin extends Component {
  constructor(props) {
    super(props);
    this.state = { redirectToReferrer: false };
  }

  login = ({ userInfo }) => {
    const { login: _login = () => ({}) } = this.context;
    const { isAuthenticated } = _login(userInfo);
    if(isAuthenticated) this.setState({ redirectToReferrer: true });
  };

  render() {
    const { location = {} } = this.props;
    const { from = { pathname: '/lab/dashboard' }, data = {} } = location.state || {};
    const { redirectToReferrer } = this.state;

    if(redirectToReferrer) return (
      <Redirect to={{
        pathname: from.pathname,
        state: { data },
      }}/>
    );

    return (
      <FullPageContainer>
        <LabLoginForm onLogin={this.login}/>
        <FooterRow />
      </FullPageContainer>
    );
  }
}

LabLogin.contextType = AuthContext;

export default LabLogin;
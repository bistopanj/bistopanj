import React, { Component } from 'react';
import { Spinner } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

import LabRegisterForm from '../../components/forms/LabRegisterForm';
import FullPageContainer from '../../components/FullPageContainer';
import { AppContext, useAppContext } from '../../contexts';
import { translateForUser, errorTypes } from '../../errors';

const pageModes = {
  initial: 0,
  ready: 1,
};

const FooterRow = () => {
  const { t } = useAppContext();
  return (
    <div style={{ marginTop: 5 }}>
      <Link to="/lab" className="footer-link">
        {t('lab.register.return-to-lab')}
      </Link>
    </div>
  );
};


const doNothing = () => ({});

class LabRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldRedirect: false,
      mode: pageModes.initial,
    };
  }

  componentDidMount = async () => {
    const { getUserLabs, isAuthenticated, showError = doNothing } = this.context;
    try {
      if(isAuthenticated){
        const labs = await getUserLabs();
        this.setState({ mode: pageModes.ready, shouldRedirect: labs.length > 0 });
      }
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(translateForUser(error));
    }
  };

  render() {
    const { mode, shouldRedirect } = this.state;
    const { isAuthenticated, t } = this.context;

    if(!isAuthenticated)
      return(
        <FullPageContainer>
          <LabRegisterForm />
          <FooterRow />
        </FullPageContainer>
      );

    if(shouldRedirect)
      return(<Redirect to="/lab/choose" />);

    if (mode === pageModes.initial)
      return (
        <FullPageContainer>
          <Spinner size="lg" color="info" />
          <br />
          <br />
          <p>{t('lab.register.checking-your-data')}</p>
        </FullPageContainer>
      );

    return(
      <FullPageContainer>
        <LabRegisterForm />
        <FooterRow />
      </FullPageContainer>
    );
  }

}

LabRegister.contextType = AppContext;

export default LabRegister;
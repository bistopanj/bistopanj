import React, { Component } from 'react';
import { Spinner } from 'reactstrap';
import { Redirect } from 'react-router-dom';

import { AppContext } from '../../contexts';
import FullPageContainer from '../../components/FullPageContainer';

class LabLogout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      goBack: false,
      proceed: false,
    };
    this.askForConfirmation = this.askForConfirmation.bind(this);
    this.logout = this.logout.bind(this);
    this.goBack = this.goBack.bind(this);
  }

  componentDidMount() {
    setTimeout(this.askForConfirmation, 0);
  }

  logout() {
    const { logout } = this.context;
    logout();
    this.setState({ proceed: true });
  };

  goBack () {
    this.setState({ goBack: true });
  };


  askForConfirmation() {
    const { openModal, t } = this.context;
    openModal({
      body: t('lab.logout.are-you-sure'),
      confirmText: t('lab.logout.confirm-logout'),
      cancelText: t('lab.logout.cancel-logout'),
      onConfirm: this.logout,
      onCancel: this.goBack,
    });
  }


  render() {
    const { goBack, proceed } = this.state;
    const { t } = this.context;
    if(goBack)
      return(
        <Redirect to="/lab/dashboard" />
      );

    if(proceed)
      return(
        <Redirect to="/lab" />
      );
    return (
      <FullPageContainer>
        <Spinner size="lg" color="info" />
        <br />
        <br />
        <p>{t('lab.logout.waiting')}</p>
      </FullPageContainer>
    );
  }
}

LabLogout.contextType = AppContext;

export default LabLogout;
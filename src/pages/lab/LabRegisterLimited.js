import React from 'react';
import { Card } from 'reactstrap';
import { Link } from 'react-router-dom';
import FullPageContainer from '../../components/FullPageContainer';
import { useAppContext } from '../../contexts';

const FooterRow = () => {
  const { t } = useAppContext();
  return (
    <div style={{ marginTop: 5 }}>
      <Link to="/lab" className="footer-link">
        {t('lab.register.return-to-lab')}
      </Link>
    </div>
  );
};

const LabRegisterLimited = () => {
  const { t } = useAppContext();
  return (
    <FullPageContainer>
      <Card className="form-card">
        <p>{t('lab.register.limited-desc')}</p>
        <p>{t('lab.register.limited-desc-2')}</p>
        <p>
          {t('lab.register.limited-ask-to-join-1')}
          <a href="mailto:hi@bistopanj.com?Subject=I am interested to use Lab">hi@bistopanj.com</a>
          {t('lab.register.limited-ask-to-join-3')}
        </p>
        <Link className="btn btn-lg btn-info" to="/lab/register">
          {t('lab.register.setup')}
        </Link>
      </Card>
      <FooterRow />
    </FullPageContainer>
  );
};

export default LabRegisterLimited;

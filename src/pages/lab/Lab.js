import React, { useContext } from 'react';
import LabPoster from '../../components/lab/LabPoster';
import LabCustomerValues from '../../components/lab/LabCustomerValues';
import LabResultSample from '../../components/lab/LabResultSample';
import LabOwnerValues from '../../components/lab/LabOwnerValues';
import LabPricing from '../../components/lab/LabPricing';
import LabDownload from '../../components/lab/LabDownload';
import NavBar from '../../components/nav/NavBar';
import labMenuItems from '../../data/lab-menu-items';
import { AuthContext } from '../../contexts/auth';

const Lab = () => {
  const { isAuthenticated } = useContext(AuthContext);
  return (
    <main role="main">
      <NavBar
        logo="lab"
        menuItems={labMenuItems[isAuthenticated ? 'loggedIn' : 'loggedOut']}
        logoLink="/lab"
        transparent
        color="purple"
      />
      <LabPoster limited/>
      <LabCustomerValues />
      <LabResultSample />
      <LabOwnerValues />
      <LabDownload />
      <LabPricing />
    </main>
  );
  
};
export default Lab;

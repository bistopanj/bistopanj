import React, { Component } from 'react';
import { Spinner } from 'reactstrap';
import { Redirect } from 'react-router-dom';

import { createLab } from '../../services/api.service';
import FullPageContainer from '../../components/FullPageContainer';
import { AppContext } from '../../contexts';
import { translateForUser, errorTypes } from '../../errors';

const pageModes = {
  initial: 0,
  ready: 1,
  done: 2
};

const doNothing = () => ({});

class LabCreate extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.state = {
      mode: pageModes.initial,
      requestSent: false,
      shouldCreate: true
    };
  }

  componentDidMount = () => {
    const { labs = [] } = this.context;
    this.setState({ mode: pageModes.ready, shouldCreate: labs.length < 1 });
  };

  componentDidUpdate = async () => {
    const { mode, requestSent, shouldCreate } = this.state;
    const {
      location: { state: { data: { labName = null } = {} } = {} } = {}
    } = this.props;

    if (mode === pageModes.ready && labName && shouldCreate && !requestSent) {
      this.setState({ requestSent: true });
      const lab = await this.createLab({ name: labName });
      if (lab) this.setState({ mode: pageModes.done });
    }
  };

  componentWillUnmount() {
    this.abortController.abort();
  }

  createLab = async ({ name = '' }) => {
    const { showError = doNothing, chooseLab, getUserLabs, t, showSuccess = doNothing } = this.context;
    try {
      const {
        userInfo: { token }
      } = this.context;
      const lab = await createLab({ token, name }, this.abortController.signal);
      showSuccess(t('lab.register.lab-created-success'));
      chooseLab({ id: lab._id });
      await getUserLabs();
      return lab;
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
      return null;
    }
  };

  render() {
    const {
      location: { state: { data: { labName = null } = {} } = {} } = {}
    } = this.props;
    const { mode, shouldCreate } = this.state;
    const { t } = this.context;

    if (!shouldCreate) return <Redirect to="/lab/choose" />;
    if (mode === pageModes.initial)
      return (
        <FullPageContainer>
          <Spinner size="lg" color="info" />
          <br />
          <br />
          <p>{t('lab.register.checking-your-data')}</p>
        </FullPageContainer>
      );
    if (mode === pageModes.done) return <Redirect to="/lab/dashboard" />;
    if (mode === pageModes.ready && shouldCreate && labName)
      return (
        <FullPageContainer>
          <Spinner size="lg" color="info" />
          <br />
          <br />
          <p>{`${t('lab.register.creating-lab')} ${labName}`}</p>
        </FullPageContainer>
      );

    return <Redirect to="/lab/register" />;
  }
}

LabCreate.contextType = AppContext;

export default LabCreate;

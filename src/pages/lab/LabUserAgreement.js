import React from 'react';
import { Helmet } from 'react-helmet';
import TextPage from '../../components/TextPage';
import { useAppContext } from '../../contexts';
import NavBar from '../../components/nav/NavBar';
import labMenuItems from '../../data/lab-menu-items';

const LabUserAgreement = () => {
  const { t, isAuthenticated } = useAppContext();
  return (
    <>
      <Helmet>
        <title> {t('nav.lab')}{' | '}{t('lab.user-agreement.title')} </title>
      </Helmet>
      <NavBar
        logo="lab"
        menuItems={labMenuItems[isAuthenticated ? 'loggedIn' : 'loggedOut']}
        logoLink="/lab"
        color="purple"
      />
      <TextPage>
        <h4>{t('lab.user-agreement.title')}</h4>
          <p>{t('lab.user-agreement.p1')}</p>
          <h5>{t('lab.user-agreement.h1')}</h5>
          <p>{t('lab.user-agreement.p2')}</p>
          <h5>{t('lab.user-agreement.h2')}</h5>
          <p>{t('lab.user-agreement.p3')}</p>
          <p>{t('lab.user-agreement.p4')}</p>
          <p>{t('lab.user-agreement.p5')}</p>
          <h5>{t('lab.user-agreement.h3')}</h5>
          <p>{t('lab.user-agreement.p6')}</p>
          <p>{t('lab.user-agreement.p7')}</p>
          <p>{t('lab.user-agreement.p8')}</p>
          <p>{t('lab.user-agreement.p9')}</p>
          <p>{t('lab.user-agreement.p10')}</p>
          <h5>{t('lab.user-agreement.h4')}</h5>
          <p>{t('lab.user-agreement.p11')}</p>
          <p>{t('lab.user-agreement.p12')}</p>
          <h5>{t('lab.user-agreement.h5')}</h5>
          <p>{t('lab.user-agreement.p13')}</p>
          <p>{t('lab.user-agreement.p14')}</p>
          <p>{t('lab.user-agreement.p15')}</p>
          <ul>
            <li>{t('lab.user-agreement.ul1-1')}</li>
            <li>{t('lab.user-agreement.ul1-2')}</li>
            <li>{t('lab.user-agreement.ul1-3')}</li>
            <li>{t('lab.user-agreement.ul1-4')}</li>
            <li>{t('lab.user-agreement.ul1-5')}</li>
          </ul>
          <p>{t('lab.user-agreement.p16')}</p>
          <h5>{t('lab.user-agreement.h6')}</h5>
          <p>{t('lab.user-agreement.p17')}</p>
          <p>{t('lab.user-agreement.p18')}</p>
          <h5>{t('lab.user-agreement.h7')}</h5>
          <p>{t('lab.user-agreement.p19')}</p>
          <h5>{t('lab.user-agreement.h8')}</h5>
          <p>{t('lab.user-agreement.p20')}</p>
          <p>{t('lab.user-agreement.p21')}</p>
          <h5>{t('lab.user-agreement.h9')}</h5>
          <p>{t('lab.user-agreement.p22')}</p>
      </TextPage>
    </>
  );
};

export default LabUserAgreement;
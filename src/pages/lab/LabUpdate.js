import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Spinner } from 'reactstrap';

import FullPageContainer from '../../components/FullPageContainer';
import { translateForUser, errorTypes } from '../../errors';
import { AppContext } from '../../contexts';

const doNothing = () => ({});

class LabUpdate extends Component {


  constructor(props) {
    super(props);
    this.abortColtroller = new AbortController();
    this.state = { redirectToReferrer: false };
  }

  componentDidMount = () => {
    this.updateLabs();
  };

  componentWillUnmount() {
    this.abortColtroller.abort();
  }

  updateLabs = async () => {
    const { getUserLabs = () => ({}), showError = doNothing, t } = this.context;
    try {
      const labs = await getUserLabs(this.abortColtroller.signal);
      if(labs) this.setState({ redirectToReferrer: true });
    } catch(error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    }
  };

  render() {
    const { location = {} } = this.props;
    const { from = { pathname: '/lab/dashboard' }, data = {} } = location.state || {};
    const { redirectToReferrer } = this.state;
    const { t } = this.context;

    if(redirectToReferrer) return (
      <Redirect to={{
        pathname: from.pathname,
        state: { data },
      }}/>
    );

    return (
      <FullPageContainer>
        <Spinner size="lg" color="info" />
        <br />
        <br />
        <p>{t('lab.register.checking-your-data')}</p>
      </FullPageContainer>
    );
  }
}

LabUpdate.contextType = AppContext;

export default LabUpdate;
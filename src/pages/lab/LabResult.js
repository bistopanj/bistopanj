import React, { useEffect, useState, useCallback } from 'react';
import { Spinner } from 'reactstrap';
import FullPageContainer from '../../components/FullPageContainer';
import { getResult } from '../../services/api.service';
import LabResultHeader from '../../components/lab/LabResultHeader';
import { useAppContext } from '../../contexts';
import { translateForUser, errorTypes } from '../../errors';
import LabResultHeadTags from '../../components/lab/LabResultHeadTags';
import LabResultGallery from '../../components/lab/LabResultGallery';

const Waiting = () => {
  const { t } = useAppContext();
  return (
    <FullPageContainer>
      <LabResultHeadTags />
      <Spinner size="lg" color="info" />
      <br />
      <br />
      <p>{t('lab.result.looking-for-result')}</p>
    </FullPageContainer>
  );
};

const LabResult = ({ match }) => {
  const [result, setResult] = useState(null);
  const [pageReady, setPageReady] = useState(false);
  const { showError, t } = useAppContext();

  const fetchResultData = useCallback(
    async abortC => {
      const { params: { resultId } = {} } = match;

      if (!resultId || !pageReady) return null;

      try {
        const theResult = await getResult({ resultId }, abortC.signal);
        setResult(theResult);
        return true;
      } catch (error) {
        if (error.name !== errorTypes.AbortError)
          showError(t(translateForUser(error)));
        return false;
      }
    },
    [match, showError, t, pageReady]
  );

  useEffect(() => {
    const abortController = new AbortController();
    fetchResultData(abortController);
    return () => {
      abortController.abort();
    };
  }, [fetchResultData]);

  useEffect(() => {
    setPageReady(true);
  }, []);

  if (!result) return <Waiting />;
  return (
    <main role="main">
      <div className="d-inline-flex flex-column min-vh-100">
        <LabResultHeadTags />
        <LabResultHeader
          labName={result.lab.name}
          labDescription={result.lab.description}
        />
        <LabResultGallery photos={result.photos} />
      </div>
    </main>
  );
};

export default LabResult;

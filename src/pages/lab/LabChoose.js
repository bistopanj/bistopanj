import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { AuthContext } from '../../contexts/auth';
import FullPageContainer from '../../components/FullPageContainer';
import LabsListCard from '../../components/lab/LabsListCard';

const defaultRedirDestination = '/lab/dashboard';

class LabChoose extends Component {
  constructor(props) {
    super(props);
    this.state = { redirectToReferrer: false };
  }

  componentDidMount = () => {
    const { labs } = this.context;
    const { location = {} } = this.props;
    const { from = { pathname: null } } = location.state || {};
    
    const fromPath = from.pathname || '';
    if(labs.length === 1 && fromPath.startsWith('/lab/dashboard')) {
      this.chooseLab({id: labs[0]._id});
    }
  };

  chooseLab = ({ id }) => {
    const { chooseLab } = this.context;
    chooseLab({ id });
    this.setState({ redirectToReferrer: true });
  };

  render() {
    const { labs = [] } = this.context;
    const { location = {} } = this.props;
    const { from = { pathname: null }, data = {} } =
      location.state || {};
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer)
      return (
        <Redirect
          to={{
            pathname: from.pathname || defaultRedirDestination,
            state: { data }
          }}
        />
      );

    if (labs.length > 0)
      return (
        <FullPageContainer>
          <LabsListCard labs={labs} onSelect={this.chooseLab}/>
        </FullPageContainer>
      );

    return <Redirect to="/lab/register" />;
  }
}

LabChoose.contextType = AuthContext;

export default LabChoose;

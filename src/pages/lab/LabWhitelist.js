import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FullPageContainer from '../../components/FullPageContainer';
import { AppContext, useAppContext } from '../../contexts';
import CardForm from '../../components/forms/CardForm';
import { parseMobileNumber } from '../../utils/tools';
import { translateForUser, errorTypes } from '../../errors';
import { addCandidate } from '../../services/api.service';

const FooterRow = () => {
  const { t } = useAppContext();
  return (
    <div style={{ marginTop: 5 }}>
      <Link to="/lab" className="footer-link" >
        {t('lab.register.return-to-lab')}
      </Link>
    </div>
  );
};

const formItems = [
  {
    name: 'mobileNumber',
    label: 'lab.login.mobile-num-label',
    id: 'whitelist-mobile-number',
    placeholder: 'lab.login.mobile-num-placeholder',
    type: 'text',
    autoFocus: true,
  }
];

const doNothing = () => ({});

class LabWhitelist extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.state = {
      isFormReady: false,
      mobileNumber: null
    };
    this.handleMobileNumberChange = this.handleMobileNumberChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.submitCandidate = this.submitCandidate.bind(this);
    this.setFormRef = this.setFormRef.bind(this);

    this.listeners = {
      mobileNumber: this.handleMobileNumberChange,
    };
    
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  onSubmit(e) {
    e.preventDefault();
    const { isFormReady } = this.state;
    if (!isFormReady) return false;
    this.submitCandidate();
    return true;
  };

  setFormRef(element) {
    this.form = element;
  };

  async submitCandidate() {
    const { mobileNumber } = this.state;
    const {
      userInfo: { token },
      showError = doNothing,
      showSuccess = doNothing,
      t,
    } = this.context;
    const { wait = doNothing, endWait = doNothing } = this.form.button || {};
    try {
      wait();
      await addCandidate({ mobileNumber, token }, this.abortController.signal);
      showSuccess('Added.');
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  }

  handleMobileNumberChange(e) {
    const text = e.target.value;
    const mobileNumber = parseMobileNumber(text);
    if (mobileNumber) {
      this.setState({
        isFormReady: true,
        mobileNumber: mobileNumber.standardFormat
      });
    } else {
      this.setState({
        isFormReady: false,
        mobileNumber: null
      });
    }
  };

  render() {
    const { isFormReady } = this.state;
    return (
      <FullPageContainer>
        <CardForm
          cardTitle="Add Candidate"
          buttonText="Add"
          formItems={formItems}
          listeners={this.listeners}
          disabled={!isFormReady}
          onSubmit={this.onSubmit}
          ref={this.setFormRef}
        />
        <FooterRow />
      </FullPageContainer>
    );
  }
}

LabWhitelist.contextType = AppContext;

export default LabWhitelist;
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NotFound from '../NotFound';
import appDownloadLinks from '../../data/mobile-app-download-links';

const RedirectExternal = ({ to = '/' }) => {
  window.location = to;
  return null;
};


const LabMobileAppRouter = () => (
  <Switch>
    <Route exact path="/lab/mobile-app/android/cafebazaar" render={ () => <RedirectExternal to={ appDownloadLinks.cafebazaar } /> } />
    <Route exact path="/lab/mobile-app/android/googleplay" render={ () => <RedirectExternal to={ appDownloadLinks.googleplay } /> } />
    <Route exact path="/lab/mobile-app/ios" render={ () => <RedirectExternal to={ appDownloadLinks.ios } /> } />
    <Route component={NotFound} />
  </Switch>
);

export default LabMobileAppRouter;

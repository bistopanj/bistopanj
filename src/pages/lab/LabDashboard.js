import React, { Component } from 'react';
import PageWithSidebar from '../../components/PageWithSidebar';
import DashboardSidebar from '../../components/dashboard/DashboardSidebar';
import DashboardNavbar from '../../components/dashboard/DashboardNavbar';
import LabDashboardRouter from './dashboard/LabDashboardRouter';
import { AuthContext } from '../../contexts';

class LabDashboard extends Component {
  constructor(props) {
    super(props);
    this.frame = null;
    this.state = {};
  }

  onItemSelect = () => {
    this.frame.onItemSelect();
  };

  toggleSidebar = () => {
    this.frame.toggle();
  };

  registerSidebarRef = element => {
    this.frame = element;
  };

  render() {
    const { labs, selectedLab } = this.context;
    const sidebar = <DashboardSidebar labs={labs} selectedLab={selectedLab} onItemSelect={this.onItemSelect}/>;
    return (
      <PageWithSidebar
        sidebar={sidebar}
        sidebarWidth="300px"
        ref={this.registerSidebarRef}
      >
        <DashboardNavbar onToggle={this.toggleSidebar}/>
        <LabDashboardRouter />
      </PageWithSidebar>
    );
  }
}

LabDashboard.contextType = AuthContext;

export default LabDashboard;

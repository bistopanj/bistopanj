import React from 'react';
import LabInfo from '../../../components/dashboard/LabInfo';
import LabDeactivator from '../../../components/dashboard/LabDeactivator';
import { useAppContext } from '../../../contexts';

const LabSettings = () => {
  const { labs, selectedLab, t } = useAppContext();
  const theLab = labs.find(lab => lab._id === selectedLab);
  return (
    <main role="main" className="dashboard-main">
      <h3 className="my-heading-font">{t('lab.dashboard.settings')}</h3>
      <p>{t('lab.dashboard.you-can-change-settings')}</p>
      <LabInfo lab={theLab} />
      <LabDeactivator lab={theLab} />
    </main>
  );
};

export default LabSettings;
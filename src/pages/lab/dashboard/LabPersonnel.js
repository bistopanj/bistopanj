import React from 'react';
import LabPersonnelManager from '../../../components/dashboard/LabPersonnelManager';
import { useAppContext } from '../../../contexts';

const LabPersonnel = () => {
  const { labs, selectedLab, t } = useAppContext(); 
  const theLab = labs.find(lab => lab._id === selectedLab);

  return (
    <main role="main" className="dashboard-main">
      <h3 className="my-heading-font">{t('lab.dashboard.manage-personnel')}</h3>
      <p>{t('lab.dashboard.manage-personnel-desc-2')}</p>
      <LabPersonnelManager lab={theLab} />
    </main>
  );
};

export default LabPersonnel;

import React from 'react';
import AppDownloadLink from '../../../components/AppDownloadLink';
import downloadAppAS from '../../../images/downloadAppAS';
import downloadAppGP from '../../../images/downloadAppGP';
import downloadAppCB from '../../../images/downloadAppCB';
import { useAppContext } from '../../../contexts';

const iconContainerStyle = {
  marginTop: '2rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  justifyContent: 'center'
};
const iconStyle = {
  marginTop: 5
};

const AppDownloadLinks = () => {
  const { i18n } = useAppContext();
  const { language = 'fa' } = i18n;

  return (
    <div style={iconContainerStyle}>
      <AppDownloadLink
        icon={downloadAppCB}
        to="/lab/mobile-app/android/cafebazaar"
        style={iconStyle}
      />
      <AppDownloadLink
        icon={downloadAppGP.langs[language]}
        to="/lab/mobile-app/android/googleplay"
        style={iconStyle}
      />
      <AppDownloadLink
        icon={downloadAppAS.langs[language]}
        to="/lab/mobile-app/ios"
        style={iconStyle}
      />
    </div>
  );
};

const MobileApps = () => {
  const { t } = useAppContext();
  return (
    <main role="main" className="dashboard-main">
      <h3 className="my-heading-font">{t('lab.dashboard.mobile-apps')}</h3>
      <p>{t('lab.dashboard.mobile-apps-desc')}</p>
      <AppDownloadLinks />
    </main>
  );
};

export default MobileApps;

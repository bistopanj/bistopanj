import React from 'react';
import LabResultsViewer from '../../../components/dashboard/LabResultsViewer';
import { useAppContext } from '../../../contexts';

const LabResults = () => {
  const { t } = useAppContext();

  return (
    <main role="main" className="dashboard-main">
      <h3 className="my-heading-font">{t('lab.dashboard.results')}</h3>
      <LabResultsViewer />
    </main>
  );
};

export default LabResults;
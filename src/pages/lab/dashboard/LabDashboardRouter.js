import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './index.css';
import NotFound from '../../NotFound';
import MobileApps from './MobileApps';
import LabDashboardHome from './LabDashboardHome';
import LabPersonnel from './LabPersonnel';
import LabResults from './LabResults';
import LabSettings from './LabSettings';

const LabDashboardRouter = () => (
  <Switch>
    <Route exact path="/lab/dashboard/" component={LabDashboardHome} />
    <Route exact path="/lab/dashboard/apps" component={MobileApps} />
    <Route exact path="/lab/dashboard/lab-personnel" component={LabPersonnel} />
    <Route exact path="/lab/dashboard/test-results" component={LabResults} />
    <Route exact path="/lab/dashboard/settings" component={LabSettings} />
    <Route component={NotFound} />
  </Switch>
);

export default LabDashboardRouter;
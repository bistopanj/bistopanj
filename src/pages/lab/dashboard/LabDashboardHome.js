import React from 'react';
import descriptionItems from '../../../contents/LabDashboardHome';
import LabPanelDescription from '../../../components/dashboard/LabPanelDescription';
import { useAppContext } from '../../../contexts';

const welcomeMessageParagraghs = [
  { text: 'lab.dashboard.welcome', key: '1' },
  { text: 'lab.dashboard.is-active', key: '2' },
  { text: 'lab.dashboard.you-can', key: '3' }
];

const WelcomeMessage = () => {
  const { t } = useAppContext();
  return (
    <>
      {welcomeMessageParagraghs.map(item => (
        <p key={`dashboard-home-${item.key}`}>{t(item.text)}</p>
      ))}
    </>
  );
};

const LabDashboardHome = () => {
  const { t } = useAppContext();
  return (
    <main role="main" className="dashboard-main">
      <h3 className="my-heading-font">{t('lab.dashboard.home')}</h3>
      <WelcomeMessage />
      <LabPanelDescription items={descriptionItems} />
    </main>
  );
};

export default LabDashboardHome;

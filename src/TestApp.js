import React, { Component, Suspense } from 'react';
import { useTranslation, withTranslation } from 'react-i18next';
import { ButtonGroup, Button } from 'reactstrap';


// class TestApp extends Component {
//   render() {
//     const { t, i18n } = this.props;
//     return (
//       <p>{t('intro')} Farid</p>
//     );
//   }
// }



const TestComponent = () => {
  const { t, i18n } = useTranslation();
  const setLang = lang => i18n.changeLanguage(lang);

  return (
    <>
      <p>{t('home.welcome')} Farid</p>
      <p>{`language: ${i18n.language}`}</p>
      <ButtonGroup>
        <Button color="primary" onClick={() => setLang('en')} >en</Button>
        <Button color="primary" onClick={() => setLang('fa')}>fa</Button>
        <Button color="primary" onClick={() => setLang('fr')}>fr</Button>
      </ButtonGroup>
    </>
  );
};

const TestApp = () => {
  return (
    <Suspense fallback="loading">
      <TestComponent />
    </Suspense>
  );
};


// export default withTranslation()(TestApp);
export default TestApp;
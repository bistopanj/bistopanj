import { buildSrcSet } from '../utils/tools';

import imageAr from '../assets/on-app-store-ar.png';
import image2xAr from '../assets/on-app-store-ar@2x.png';
import image3xAr from '../assets/on-app-store-ar@3x.png';

import imageEn from '../assets/on-app-store-en.png';
import image2xEn from '../assets/on-app-store-en@2x.png';
import image3xEn from '../assets/on-app-store-en@3x.png';

import imageFr from '../assets/on-app-store-fr.png';
import image2xFr from '../assets/on-app-store-fr@2x.png';
import image3xFr from '../assets/on-app-store-fr@3x.png';

const setAr = {
  '2x': image2xAr,
  '3x': image3xAr,
};

const setEn = {
  '2x': image2xEn,
  '3x': image3xEn,
};

const setFr = {
  '2x': image2xFr,
  '3x': image3xFr
};

const downloadAppAS = {
  src: imageEn,
  srcSet: buildSrcSet(setEn),
  alt: 'get the app on the iOS App Store',
  langs: {
    'fa': {
      src: imageEn,
      srcSet: buildSrcSet(setEn),
      alt: 'دانلود نرم‌افزار iOS از اپ استور اپل',
    },
    'ar': {
      src: imageAr,
      srcSet: buildSrcSet(setAr),
      alt: 'تنزیل من App Store',
    },
    'en': {
      src: imageEn,
      srcSet: buildSrcSet(setEn),
      alt: 'get the app on the App Store',
    },
    'fr': {
      src: imageFr,
      srcSet: buildSrcSet(setFr),
      alt: "Télécharger dans l'App Store",
    },
  },
};

export default downloadAppAS;

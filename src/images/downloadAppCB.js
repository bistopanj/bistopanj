import { buildSrcSet } from '../utils/tools';

import image from '../assets/on-cafebazaar.png';
import image2x from '../assets/on-cafebazaar@2x.png';
import image3x from '../assets/on-cafebazaar@3x.png';

const set = {
  '2x': image2x,
  '3x': image3x
};

const downloadAppCB = {
  src: image,
  srcSet: buildSrcSet(set),
  alt: 'دانلود نرم‌افزار موبایل از کافه بازار'
};

export default downloadAppCB;

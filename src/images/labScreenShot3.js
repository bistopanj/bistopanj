import { buildSrcSet } from '../utils/tools';

import image from '../assets/ms_confirm.png';
import image2x from '../assets/ms_confirm@2x.png';
import image3x from '../assets/ms_confirm@3x.png';

const set = {
  '2x': image2x,
  '3x': image3x
};

const labScreenShot3 = {
  src: image,
  srcSet: buildSrcSet(set),
  alt: 'تصویر صفحه‌ی تایید نهایی و ارسال در نرم‌افزار موبایل لب'
};

export default labScreenShot3;

import { buildSrcSet } from '../utils/tools';

import imageFa from '../assets/on-google-play-fa.png';
import image2xFa from '../assets/on-google-play-fa@2x.png';
import image3xFa from '../assets/on-google-play-fa@3x.png';

import imageAr from '../assets/on-google-play-ar.png';
import image2xAr from '../assets/on-google-play-ar@2x.png';
import image3xAr from '../assets/on-google-play-ar@3x.png';

import imageEn from '../assets/on-google-play-en.png';
import image2xEn from '../assets/on-google-play-en@2x.png';
import image3xEn from '../assets/on-google-play-en@3x.png';

import imageFr from '../assets/on-google-play-fr.png';
import image2xFr from '../assets/on-google-play-fr@2x.png';
import image3xFr from '../assets/on-google-play-fr@3x.png';

const setFa = {
  '2x': image2xFa,
  '3x': image3xFa,
};

const setAr = {
  '2x': image2xAr,
  '3x': image3xAr,
};

const setEn = {
  '2x': image2xEn,
  '3x': image3xEn,
};

const setFr = {
  '2x': image2xFr,
  '3x': image3xFr
};

const downloadAppGP = {
  src: imageFa,
  srcSet: buildSrcSet(setFa),
  alt: 'دانلود نرم‌افزار اندروید از فروشگاه گوگل',
  langs: {
    'fa': {
      src: imageFa,
      srcSet: buildSrcSet(setFa),
      alt: 'دانلود نرم‌افزار اندروید از فروشگاه گوگل',
    },
    'ar': {
      src: imageAr,
      srcSet: buildSrcSet(setAr),
      alt: 'تنزیل من Google Play',
    },
    'en': {
      src: imageEn,
      srcSet: buildSrcSet(setEn),
      alt: 'get the android app on the Google Play',
    },
    'fr': {
      src: imageFr,
      srcSet: buildSrcSet(setFr),
      alt: "Téléchargez l'application Android sur Google Play",
    },
  },
};

export default downloadAppGP;

import { buildSrcSet } from '../utils/tools';

import image from '../assets/ms_home.png';
import image2x from '../assets/ms_home@2x.png';
import image3x from '../assets/ms_home@3x.png';

const set = {
  '2x': image2x,
  '3x': image3x
};

const labScreenShot1 = {
  src: image,
  srcSet: buildSrcSet(set),
  alt: 'تصویر صفحه‌ی اول نرم‌افزار موبایل لب'
};

export default labScreenShot1;

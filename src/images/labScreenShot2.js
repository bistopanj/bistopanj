import { buildSrcSet } from '../utils/tools';

import imageFa from '../assets/ms-final-confirm-fa.png';
import image2xFa from '../assets/ms-final-confirm-fa@2x.png';
import image3xFa from '../assets/ms-final-confirm-fa@3x.png';

import imageAr from '../assets/ms-final-confirm-ar.png';
import image2xAr from '../assets/ms-final-confirm-ar@2x.png';
import image3xAr from '../assets/ms-final-confirm-ar@3x.png';

import imageEn from '../assets/ms-final-confirm-en.png';
import image2xEn from '../assets/ms-final-confirm-en@2x.png';
import image3xEn from '../assets/ms-final-confirm-en@3x.png';

import imageFr from '../assets/ms-final-confirm-fr.png';
import image2xFr from '../assets/ms-final-confirm-fr@2x.png';
import image3xFr from '../assets/ms-final-confirm-fr@3x.png';

const setFa = {
  '2x': image2xFa,
  '3x': image3xFa
};

const setAr = {
  '2x': image2xAr,
  '3x': image3xAr
};

const setEn = {
  '2x': image2xEn,
  '3x': image3xEn
};

const setFr = {
  '2x': image2xFr,
  '3x': image3xFr
};

const labScreenShot2 = {
  src: imageFa,
  srcSet: buildSrcSet(setFa),
  alt: 'تصویر نرم افزار موبایل لب',
  langs: {
    fa: {
      src: imageFa,
      srcSet: buildSrcSet(setFa),
      alt: 'تصویر نرم افزار موبایل لب'
    },
    ar: {
      src: imageAr,
      srcSet: buildSrcSet(setAr),
      alt: 'لقطة شاشة لتطبيق المختبر المحمول'
    },
    en: {
      src: imageEn,
      srcSet: buildSrcSet(setEn),
      alt: 'screenshot of the lab mobile app'
    },
    fr: {
      src: imageFr,
      srcSet: buildSrcSet(setFr),
      alt: "capture d'écran de l'application mobile de laboratoire"
    }
  }
};

export default labScreenShot2;

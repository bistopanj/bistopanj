import downloadAppAS from './downloadAppAS';
import downloadAppCB from './downloadAppCB';
import downloadAppGP from './downloadAppGP';
import labScreenShot1 from './labScreenShot1';
import labScreenShot2 from './labScreenShot2';
import labScreenShot3 from './labScreenShot3';

export {
  downloadAppAS,
  downloadAppCB,
  downloadAppGP,
  labScreenShot1,
  labScreenShot2,
  labScreenShot3
};

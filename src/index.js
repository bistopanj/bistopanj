import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';
import 'abortcontroller-polyfill/dist/abortcontroller-polyfill-only';
import 'bootstrap/dist/css/bootstrap.min.css';


import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';
import './index.css';
import './i18n';
import App from './App';

WebFont.load({
  google: {
    families: [ 'Ubuntu', 'Open Sans'],
  },
});

ReactDOM.render(<App /> , document.getElementById('root'));

import errorCodes from './error-codes';
import { trackException } from '../contexts/tracking';

const translateForUser = (error) => {
  // try {
  //   return errorCodes[error.code].toUser;
  // } catch (err) {
  //   return errorCodes[9999].toUser;
  // }
  const { code = 9999 } = error || {};
  trackException({ description: `${code}` });
  if(code in errorCodes ) return `errors.${code}`;
  return 'errors.9999';
};

export default translateForUser;
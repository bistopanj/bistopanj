class ServerError extends Error {
  constructor(props) {
    const errorType = 'ServerError';
    if(typeof props === 'string')
    {
      super(props);
      this.name = errorType;
      this.type = errorType;
      this.message = props;
      this.code = 9997;
    } else {
      const { status, code, type, message = "", moreInfo = "" } = props || {};
      super(message);
      this.type = errorType;
      this.name = errorType;
      this.serverType = type;
      this.code = code;
      this.status = status;
      this.moreInfo = moreInfo;
      this.message = message;
    }
  }
}

export default ServerError;
import errorTypes from './error-types';

const errorCodes = {
  // codes from 100 to 999 are reserved for server errors.
  100: {
    toUser:
      'شماره‌ی موبایلی که وارد کردید معتبر نیست. لطفا از درست بودن آن مطمئن شوید'
  },
  101: {
    toUser:
      'مشکلی در سیستم ارسال پیامک وجود دارد. لطفا چند دقیقه دیگر دوباره تلاش کنید'
  },
  102: {
    toUser: 'برای ورود باید شماره موبایل معتبری ارائه کنید'
  },
  103: {
    toUser:
      'رمزی که وارد کردید اشتباه است. لطفا به پیامکی که برایتان ارسال شده مراجعه کنید'
  },
  104: {
    toUser:
      'به دلیل بروز مشکلی در سیستم، قادر به بررسی صحت اطلاعات شما نبودیم. لطفا بعد از چند دقیقا دوباره تلاش کنید'
  },
  105: { toUser: 'مشکلی پیش آمد. پوزش می‌خواهیم' },
  106: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
  107: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
  108: { toUser: 'شما دسترسی لازم را برای استفاده از این سامانه ندارید' },
  109: { toUser: 'ورودی مورد نظر به شکل درستی وارد نشده است' },
  110: { toUser: 'شما دسترسی لازم برای این کار را ندارید' },
  111: { toUser: 'آزمایشگاه مورد نظر وجود در سامانه وجود ندارد' },
  112: {
    toUser: 'اطلاعات لازم برای تغییر در آزمایشگاه به درستی وارد نشده است'
  },
  113: {
    toUser:
      'شما به سقف تعداد کاربر آزمایشگاه‌تان رسیده‌اید. لطفا برای ارتقاء حساب کاربری‌تان با ما تماس بگیرید'
  },
  114: {
    toUser:
      'به دلیل مشکلی در سیستم پیامک نتوانستیم این نتیجه را برای بیمار ارسال کنیم'
  },
  115: { toUser: 'اطلاعات وارد شده برای دریافت نتیجه آزمایش اشتباه است' },
  116: {
    toUser:
      'به دلیل مشکلی در سیستم بارگزاری عکس نتوانستیم این کار را انجام دهیم. لطفا بعد از چند دقیقه دوباره تلاش کنید'
  },
  117: { toUser: 'اطلاعات وارد شده برای نتیجه آزمایش اشتباه است' },
  118: { toUser: 'نتیجه ی خواسته شده در سیستم موجود نیست' },
  119: { toUser: 'اطلاعات وارد شده برای اضافه کردن کاربر به لیست سفید صحیح وارد نشده.'},
  120: { toUser: 'در حال حاضر استفاده از این سامانه محدود به کاربرانی است که دعوت شده‌اند. در صورتی که می‌خواهید در این مرحله از سیستم استفاده کنید با نشانی hi@bistopanj.com تماس بگیرید.' },
  404: { toUser: 'یافت نشد' },
  999: { toUser: 'مشکلی در سیستم پیش آمد. متاسفیم' },
  // client errors:
  3000: {
    description:
      'This error happens when trying to get an otp from server fails and the reason is not serverError.',
    message: 'Unable to get otp from server.',
    moreInfo: 'Maybe no internet connection',
    toUser:
      'ارتباط با سرور با مشکلی مواجه است. لطفا از وصل بودن ارتباط خود با اینترنت مطمئن شوید',
    type: errorTypes.DeviceError,
  },
  3001: {
    description:
      'This error happens when trying to log in fails and the reason is not serverError.',
    message: 'Unable to login.',
    moreInfo: 'Maybe no internet connection',
    toUser:
      'ورود به سیستم با مشکلی مواجه شد. لطفا ااز وصل بودن ارتباط اینترنت خود مطمئن شوید',
    type: errorTypes.DeviceError,
  },
  3002: {
    description:
      'This happens when trying to fetch user info from device storage.',
    message: 'Unable to fetch user info from device',
    moreInfo: 'It may be related to app permissions',
    toUser:
      'مشکلی در دریافت اطلاعات شما از روی گوشی پیش آمد. لطفا مطمئن شوید که دسترسی‌های لازم را برای خواندن اطلاعات از حافظه‌ی گوشی به نرم‌افزار داده‌اید.',
    type: errorTypes.DeviceError,
  },
  3003: {
    description:
      'This happens at log out when trying to remove user info from device storage.',
    message: 'Unable to remove user info from device',
    moreInfo: 'It may be related to app permissions',
    toUser:
      'در هنگام خروج از سیستم مشکلی پیش آمد. لطفا مطمئن شوید که دسترسی‌های لازم برای خواندن اطلاعات از حافظه‌ی گوشی به نرم‌افزار داده‌شده است.',
    type: errorTypes.DeviceError,
  },
  3004: {
    description:
      'This happens when trying to get the labs owned by the user from the server. The error is not server error.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'در هنگام دریافت اطلاعات شما از مشکلی پیش آمد. لطفا از اتصال اینترنت خود مطمئن شوید.',
    type: errorTypes.DeviceError,
  },
  3005: {
    description:
      'This happens when we are unable to create a lab for the user and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3006: {
    description:
      'This happens when we are unable to add user to the lab and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3007: {
    description:
      'This happens when we are unable to remove lab user and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3008: {
    description: 'This happens when the input for mobile number is too short',
    message:
      'Length of the input string is too short to be a valid phone number',
    moreInfo:
      'A valid mobile number should be at least 10 digits. excluding the country code.',
    toUser: 'مقداری که برای شماره موبایل وارد شده است کم‌تر از حد مجاز است',
    type: errorTypes.ValidationError,
  },
  3009: {
    description: 'This happens when the input for mobile number is not valid',
    message: 'The input string is not a valid phone number',
    moreInfo:
      'The input should be a valid combination of numbers and special characters.',
    toUser: 'مقداری که برای شماره موبایل وارد شده قابل قبول نیست',
    type: errorTypes.ValidationError,
  },
  3010: {
    description:
      'This happens when the the input to a method is expected to be string but it is not',
    message: 'Invalid type of input. expected a string',
    moreInfo: 'The input should be a string',
    toUser: 'ورودی در شکل مناسبی وارد نشده است',
    type: errorTypes.TypeError,
  },
  3011: {
    description:
      'This happens when the the input to the otp input is shorter than the minimum length of otp.',
    message: 'The input is too short to be a valid one-time password',
    moreInfo: 'Please enter the password sent to your mobile number',
    toUser: 'رمز وارد شده کوتاه‌تر از مقدار مجاز است',
    type: errorTypes.ValidationError,
  },
  3012: {
    description: 'This happens when a non-numeric value is given for otp.',
    message: 'The One-time password is always a number',
    moreInfo: 'Please enter the password sent to your mobile number',
    toUser: 'رمز حتما باید یک عدد باشد',
    type: errorTypes.ValidationError,
  },
  3013: {
    description:
      'This happens when we are unable to get lab user and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3014: {
    description:
      'This happens when we are unable to get lab results and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3015: {
    description:
      'This happens when we are unable to edit lab info and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3016: {
    description:
      'This happens when we are unable to get result info and the error is not related to server.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3017: {
    description:
      'This happens when we are unable to get an image because no internet connection was available.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  3018: {
    description:
      'This happens when we are unable to deactivate lab because no internet connection was available.',
    moreInfo: 'Maybe no internet connection.',
    toUser:
      'ارتباط با سرور ممکن نیست. لطفا اتصال اینترنت خود را بررسی کنید.',
    type: errorTypes.DeviceError,
  },
  9996: {
    description: 'This happens when for some unknown reason a request to a server other than ours is failed. The problem is not internet connection.',
    moreInfo: 'Something probably is wrong with the url.',
    toUser: 'دریافت فایل با مشکل مواجه شد.',
    type: errorTypes.ServerError,
  },
  9997: {
    description: 'This is the error code for all errors that are raised without providing an error code.',
    moreInfo: 'No Error Code is specified.',
    toUser: 'مشکل نامشخصی پیش آمد.',
    type: errorTypes.UnknownError,
  },
  9998: {
    description: 'This is the error code for AbortErrors that can be ignored.',
    moreInfo: 'Nothing',
    toUser: 'درخواست لغو شد.',
    type: errorTypes.AbortError,
  },
  9999: {
    description: 'This is the general code for all unknow errors.',
    message: 'Unknown error',
    moreInfo: "We don't know the reason yet",
    toUser: 'مشکل نامشخصی پیش آمد.',
    type: errorTypes.UnknownError
  }
};

export default errorCodes;

class UnknownError extends Error {
  constructor(props) {
    const type = 'UnknownError';
    if(typeof props === 'string')
    {
      super(props);
      this.name = type;
      this.type = type;
      this.message = props;
      this.code = 9997;
    } else {
      const { message, code } = props || {};
      super(message);
      this.name = type;
      this.type = type;
      this.message = message;
      this.code = code;
    }
  }
}

export default UnknownError;

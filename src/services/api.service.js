import constants from '../constants';
import { buildQueryString } from '../utils/tools';
import { ServerError, errorTypes, DeviceError, AbortError } from '../errors';

const getOtp = async ({ mobileNumber }, signal = null) => {
  const getOtpUrl = `${constants.appParams.serverBaseAddress}auth/otp`;

  try {
    const response = await fetch(getOtpUrl, {
      method: 'POST',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ mobileNumber })
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson.data;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getOtp request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3000,
        message:
          error.message || 'Error happened when trying to get otp from server.'
      });
    }
  }
};

const login = async ({ mobileNumber, otp }, signal = null) => {
  const loginpUrl = `${constants.appParams.serverBaseAddress}auth/login`;
  try {
    const response = await fetch(loginpUrl, {
      method: 'POST',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ mobileNumber, otp })
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'login request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3001,
        message:
          error.message || 'Error happened when trying to login to server.'
      });
    }
  }
};

const getUserLabs = async ({ token }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs`;

  try {
    const response = await fetch(url, {
      method: 'GET',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getUserLabs request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3004,
        message:
          error.message || 'Error happened when trying to get user labs from server.'
      });
    }
  }
};

const createLab = async ({ token, name }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs`;
  try {
    const response = await fetch(url, {
      method: 'POST',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ name })
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'createLab request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3005,
        message:
          error.message || 'Error happened when trying to create lab.'
      });
    }
  }
};

const addUserToLab = async ({ token, user, labId }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/users`;
  try {
    const response = await fetch(url, {
      method: 'POST',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(user)
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson.data;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'addUserToLab request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3006,
        message:
          error.message || 'Error happened when trying to add user to lab.'
      });
    }
  }
};

const addCandidate = async ({ token, mobileNumber }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/candidates`;
  try {
    const response = await fetch(url, {
      method: 'POST',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ mobileNumber })
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'addUserToLab request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3006,
        message:
          error.message || 'Error happened when trying to add user to lab.'
      });
    }
  }
};

const getLabUser = async ({ token, userId, labId }) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/users/${userId}`;
  try {
    const response = await fetch(url, {
      method: 'GET',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getLabUser request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3013,
        message:
          error.message || 'Error happened when trying to get lab user.'
      });
    }
  }
};

const removeLabUser = async ({ token, userId, labId }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/users/${userId}`;
  try {
    const response = await fetch(url, {
      method: 'DELETE',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'removeLabUser request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3007,
        message:
          error.message || 'Error happened when trying to remove lab user.'
      });
    }
  }
};

const getLabResults = async ({ token, query = null, labId }, signal = null) => {
  const queryString = query ? `?${buildQueryString(query)}` : '';
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/results${queryString}`;
  try {
    const response = await fetch(url, {
      method: 'GET',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getLabResults request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3014,
        message:
          error.message || 'Error happened when trying to get lab results.'
      });
    }
  }
};

const editLabInfo = async ({ token, labId, data }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}`;
  try {
    const response = await fetch(url, {
      method: 'PUT',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'editLabInfo request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3015,
        message:
          error.message || 'Error happened when trying to edit lab info.'
      });
    }
  }
};

const deactivateLab = async ({ token, labId }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/labs/${labId}/deactivate`;
  try {
    const response = await fetch(url, {
      method: 'PUT',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'deactivateLab request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3018,
        message:
          error.message || 'Error happened when trying to deactivate lab.'
      });
    }
  }
};

const getResult = async ({ resultId }, signal = null) => {
  const url = `${constants.appParams.serverBaseAddress}api/results/${resultId}`;
  try {
    const response = await fetch(url, {
      method: 'GET',
      signal,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) return responseJson;
    throw new ServerError({ status: response.status, ...responseJson.error });
  } catch (error) {
    if (error.type === errorTypes.ServerError) throw error;
    else if (error.name === errorTypes.AbortError) {
      throw new AbortError({
        code: 9998,
        message: error.message || 'getResult request aborted by user.'
      });
    } else {
      throw new DeviceError({
        code: 3016,
        message:
          error.message || 'Error happened when trying to get result info.'
      });
    }
  }
};

export {
  getOtp,
  login,
  getUserLabs,
  createLab,
  addUserToLab,
  getLabUser,
  removeLabUser,
  getLabResults,
  getResult,
  editLabInfo,
  deactivateLab,
  addCandidate,
};

// import cloudinary from 'cloudinary-core';

const cloudName = 'nimvajabicloud';
// const cl = new cloudinary.Cloudinary({ cloud_name : 'nimvajabicloud' });
const baseUrl = `https://res.cloudinary.com/${cloudName}/image/upload/`;

const getFileName = (url) => {
  const fileName = url.split('/').pop();
  return fileName;
};

const applyTransform = ({ url, transform = null }) => {
  if(!transform) return url;

  const fileName = getFileName(url);
  return `${baseUrl}${transform}/${fileName}`;
};


const getEcoImage = url => applyTransform({ url, transform: 'q_60,w_1600' });
const getHqImage = url => applyTransform({ url, transform: 'q_100' });

export { getFileName,getHqImage, getEcoImage };
import jwtDecode from 'jwt-decode';
import constants from '../constants';

const { cookieKeys, userIdCookieExpDays } = constants.appParams;
const { token: tokenKey, userId: userIdKey } = cookieKeys;
/*
  We have only two cookie keys:
  'userId'
  'token'
  The token cookie is used to determine whether the user is logged in. 
  The userId is used to identify user even when logged out.
*/

/*
  This method checks to see if a given jwt token is valid
*/
const isTokenValid = ({ token, userId }) => {
  try {
    const tokenPayload = jwtDecode(token);
    const { user, exp } = tokenPayload;
    const { _id: tokenUserId } = user;
    const tokenExpDate = new Date(exp*1000);
    const nowDate = new Date();

    return (tokenExpDate.getTime() > nowDate.getTime()) && (tokenUserId === userId);
  } catch (error) {
    return false;
  }
};

/*
  This method looks for the userInfo in cookies and returns it.
*/
const checkLogin = (cookies) => {
  // TODO: check this token with server to ensure that it is still valid.
  const userId = cookies.get(userIdKey, { path: '/' });
  const token = cookies.get(tokenKey, { path: '/' });  
  if(!userId || !token) return {userInfo: null, isAuthenticated: false }

  try {
    const isAuthenticated = isTokenValid({ token, userId });
    const userInfo = { userId, token };
    return { userInfo, isAuthenticated };
  } catch (error) {
    return {userInfo: null, isAuthenticated: false }
  }
};

const logout = ({ cookies }) => {
  try {
    cookies.remove(tokenKey, { path: '/' });
    return true;
  } catch(error) {
    console.log(error);
  }
};

/*
  This method saves the userInfo on cookies.
  Sets the expiration time based on the token expiration.
*/
const login = ({ userInfo = {}, cookies }) => {
  try {
    const { token } = userInfo;
    const tokenPayload = jwtDecode(token);
    const { user, exp } = tokenPayload;
    const { _id: userId } = user;
    const isValidToken = isTokenValid({ token, userId });
    if(!isValidToken) throw new Error ('Invalid Token is provided to the auth.service.login method.');
    const tokenExpDate = new Date(exp*1000);
    const nowDate = new Date();
    const userExpDate = new Date(nowDate.getTime() + userIdCookieExpDays*86400000);
    cookies.set(tokenKey, token, {
      path: '/',
      expires: tokenExpDate,
    });
    cookies.set(userIdKey, userId, {
      path: '/',
      expires: userExpDate,
    });

    const _userInfo = { userId, token };
    const isAuthenticated = isValidToken;
    return { userInfo: _userInfo, isAuthenticated };
  } catch(error) {
    // TODO: handle errors
    console.log(error);
    return { userInfo: null, isAuthenticated: false };
  }
};

export { checkLogin, login, logout };
import colors from './colors';

const navbarThemes = {
  transparent: {
    large: {
      bg: 'transparent',
      textColor: 'white',
      underlineColor: 'white',
      togglerClassName: 'navbar-toggler-icon purple',
      navbarItemsClassName: 'mynavbar-item transparent',
      navbarClassName: 'my-navbar py-2'
    },
    small: {
      bg: 'white',
      textColor: 'black',
      underlineColor: colors.labPurple,
      togglerClassName: 'navbar-toggler-icon black-purple',
      navbarItemsClassName: 'mynavbar-item transparent',
      navbarClassName: 'my-navbar py-0 shadow'
    }
  },
  light: {
    large: {
      bg: 'white',
      textColor: 'black',
      underlineColor: colors.labPurple,
      togglerClassName: 'navbar-toggler-icon black',
      navbarItemsClassName: 'mynavbar-item light',
      navbarClassName: 'my-navbar py-2'
    },
    small: {
      bg: 'white',
      textColor: 'black',
      underlineColor: colors.labPurple,
      togglerClassName: 'navbar-toggler-icon black',
      navbarItemsClassName: 'mynavbar-item light',
      navbarClassName: 'my-navbar py-0 shadow'
    }
  },
  default: {
    large: {
      bg: 'white',
      textColor: 'black',
      underlineColor: 'black',
      togglerClassName: 'navbar-toggler-icon black',
      navbarItemsClassName: 'mynavbar-item light',
      navbarClassName: 'my-navbar py-2'
    },
    small: {
      bg: 'white',
      textColor: 'black',
      underlineColor: 'black',
      togglerClassName: 'navbar-toggler-icon black',
      navbarItemsClassName: 'mynavbar-item light',
      navbarClassName: 'my-navbar py-0 shadow'
    }
  }
};

export default navbarThemes;

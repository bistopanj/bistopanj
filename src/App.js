import React, { Suspense, useState, useEffect } from 'react';
// import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from "history";
import { useTranslation } from 'react-i18next';

import './App.css';
import Loading from './components/Loading';
import NavBar from './components/nav/NavBar';
import DefaultHeadTags from './components/company/DefaultHeadTags';
import Home from './pages/Home';
import Contacts from './pages/Contacts';
import LabRouter from './pages/LabRouter';
import Privacy from './pages/Privacy';
import NotFound from './pages/NotFound';
import Footer from './components/Footer';
import { AppContextProvider } from './contexts';
import { langs } from './constants';
import { initTracking, listenToLocationChange, trackPageview } from './contexts/tracking';

initTracking();
const findAppDirection = lng => (lng in langs) ? (langs[lng].dir) : 'ltr';
const history = createBrowserHistory();
history.listen(listenToLocationChange);

const AppComponent = () => {
  const [ initialLoad, setInitialLoad ] = useState(true);
  const { i18n } = useTranslation();
  const appDirection = findAppDirection(i18n.language);
  const appClassName = appDirection === 'ltr' ? 'App' : 'App rtl';

  useEffect(() => {
    if(initialLoad) {
      trackPageview();
      setInitialLoad(false);
    }
  }, [ initialLoad ]);

  return (
    <div className={appClassName} >
      <Router history={history}>
        <AppContextProvider>
          <DefaultHeadTags/>
          <NavBar exclude="/lab" />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/contacts" component={Contacts} />
            <Route path="/lab" component={LabRouter} />
            <Route path="/privacy" component={Privacy} />
            <Route component={NotFound} />
          </Switch>
          <Footer exclude="/lab/:sub(register|login|dashboard|create|choose|update|logout|result|mobile-app|register-limited|whitelist)" />
        </AppContextProvider>
      </Router>
    </div>
  );
};

const App = () => (
  <Suspense fallback={<Loading />} >
    <AppComponent />
  </Suspense>
);

export default App;
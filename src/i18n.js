import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-xhr-backend';


const detectionOptions = {
  order: ['cookie', 'htmlTag'],
  caches: ['cookie'],
  lookupCookie: 'lang',
};

// console.log('public: ', process.env.REACT_APP_PUBLIC_URL);
// const publicUrl = process.env.REACT_APP_PUBLIC_URL || '';
// const publicUrl = process.env.REACT_APP_PUBLIC_URL || '';

const backendOptions = {
  // loadPath:  `${publicUrl}/locales/{{lng}}/{{ns}}`,
  // addpath: `${publicUrl}/locales/add/{{lng}}/{{ns}}`,
};

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    detection: detectionOptions,
    backend: backendOptions,
    fallbackLng: 'en',
    debug: false,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
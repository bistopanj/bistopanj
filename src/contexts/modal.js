import React, { createContext, useContext, Component } from 'react';
import SharedModal from '../components/SharedModal';

const Ctx = createContext();

export class ModalProvider extends Component {

  constructor(props) {
    super(props);
    this.modal = null;
    this.setModalRef = this.setModalRef.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  setModalRef(element) {
    this.modal = element;
  }
  
  open(arg) {
    const { open: openModal = () => ({}) } = this.modal || {};
    openModal(arg);
  }

  close(arg) {
    const { closee: closeModal = () => ({}) } = this.modal || {};
    closeModal(arg);
  }

  render(){ 
    const { children, direction = 'ltr' } = this.props;
    return (
      <Ctx.Provider value={{ openModal: this.open, closeModal: this.close }}>
        {children}
        <SharedModal ref={this.setModalRef} direction={direction} />
      </Ctx.Provider>
    );
  }
}

export const useModal = () => useContext(Ctx);
export const ModalContext = Ctx;
export const ModalConsumer = Ctx.Consumer;
import ReactGA from "react-ga";
import { appParams } from '../constants';

const { googleAnalyticsTrackingId } = appParams;

export const initTracking = () => {
  ReactGA.initialize(googleAnalyticsTrackingId);
};

export const trackPageview = (location = window.location.pathname + window.location.search ) => {
  ReactGA.set({ page: location });
  ReactGA.pageview(location);
};

export const trackEvent = ({ category, action, label }) => {
  ReactGA.event({ category, action, label });
};

export const trackException = ({ description = 'unspecified error', fatal = false}) => {
  ReactGA.exception({ description, fatal });
};

export const listenToLocationChange = (location) => {
  trackPageview(location.pathname);
};
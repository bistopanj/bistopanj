import React, { createContext, useContext, Component } from 'react';
import Toasts from '../components/Toasts';

const Ctx = createContext();

export class ToastProvider extends Component {
  constructor(props) {
    super(props);
    this.toastCount = 0;
    this.state = { toasts: [] };
    this.addToast = this.addToast.bind(this);
    this.removeToast = this.removeToast.bind(this);
    this.showSuccess = this.showSuccess.bind(this);
    this.showInfo = this.showInfo.bind(this);
    this.showError = this.showError.bind(this);
    this.showWarning = this.showWarning.bind(this);
    this.show = this.show.bind(this);
  }

  show(content, options = {}) {
    const { color, ...rest } = options;
    this.addToast(content, { color: 'primary', ...rest });
  }

  showSuccess(content, options = {}) {
    const { color, ...rest } = options;
    this.addToast(content, { color: 'success', ...rest });
  }

  showInfo(content, options = {}) {
    const { color, ...rest } = options;
    this.addToast(content, { color: 'info', ...rest });
  }

  showError(content, options = {}) {
    const { color, ...rest } = options;
    this.addToast(content, { color: 'danger', ...rest });
  }

  showWarning(content, options = {}) {
    const { color, ...rest } = options;
    this.addToast(content, { color: 'warning', ...rest });
  }

  addToast(content, options = {}) {
    this.toastCount += 1;
    const id = this.toastCount;
    const toast = { content, id, options };
    this.setState(prevState => ({ toasts: [...prevState.toasts, toast]}));
  }

  removeToast(id) {
    this.setState(prevState => ({ toasts: prevState.toasts.filter(t => t.id !== id)}));
  }

  render() {
    
    const { children } = this.props;
    const { toasts } = this.state;
    const methods = {
      add: this.addToast,
      remove: this.removeToast,
      show: this.show,
      showInfo: this.showInfo,
      showSuccess: this.showSuccess,
      showWarning: this.showWarning,
      showError: this.showError,
    };
    return (
      <Ctx.Provider value={methods}>
        {children}
        <Toasts toasts={toasts} onDismiss={this.removeToast}/>
      </Ctx.Provider>
    );
  }
}

export const useToasts = () => useContext(Ctx);
export const ToastContext = Ctx;
export const ToastConsumer = Ctx.Consumer;
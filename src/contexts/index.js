import React, { createContext, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { ToastContext, ToastProvider, ToastConsumer, useToasts } from './toast';
import { AuthContext, AuthProvider, AuthConsumer, useAuth } from './auth';
import { ModalContext, ModalProvider, ModalConsumer, useModal } from './modal';
import ScrollToTop from '../components/ScrollToTop';
import { langs } from '../constants';

const AppContext = createContext();
const AppContextConsumer = AppContext.Consumer;

const findAppDirection = lng => (lng in langs) ? (langs[lng].dir) : 'ltr';

const AppContextProvider = ({ children }) => {
  const { t , i18n } = useTranslation();
  const direction = findAppDirection(i18n.language);


  return (
    <ToastProvider>
      <ModalProvider direction={direction}>
        <ToastConsumer>
          {toastContext => (
            <AuthProvider showError={toastContext.showError} >
              <ModalConsumer>{modalContext => (
                <AuthConsumer>
                  {authContext => (
                    <AppContext.Provider value={{ ...authContext, ...toastContext, ...modalContext, direction, t, i18n }}>
                      <ScrollToTop>
                        {children}
                      </ScrollToTop>
                    </AppContext.Provider>
                  )}
                </AuthConsumer>
              )}
              </ModalConsumer>
            </AuthProvider>
          )}
        </ToastConsumer>
      </ModalProvider>
    </ToastProvider>
  );
};

const useAppContext = () => useContext(AppContext);
export default AppContext;
export {
  AppContext,
  AppContextConsumer,
  AppContextProvider,
  useAppContext,
  ToastContext,
  ToastProvider,
  ToastConsumer,
  useToasts,
  AuthContext,
  AuthProvider,
  AuthConsumer,
  useAuth,
  ModalConsumer,
  ModalContext,
  ModalProvider,
  useModal,
};

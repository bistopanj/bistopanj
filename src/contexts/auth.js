import React, { Component, createContext, useContext } from 'react';
import Cookies from 'universal-cookie';
import {
  checkLogin,
  login as _login,
  logout as _logout
} from '../services/auth.service';
import { getUserLabs as getUserLabsServ } from '../services/api.service';
import { translateForUser, errorTypes } from '../errors';

const AuthContext = createContext();
const AuthConsumer = AuthContext.Consumer;

const useAuth = () => useContext(AuthContext);

export class AuthProvider extends Component {
  constructor(props) {
    super(props);
    this.cookies = new Cookies();
    const { userInfo, isAuthenticated } = checkLogin(this.cookies);
    this.state = {
      userInfo,
      isAuthenticated,
      labs: null,
      selectedLab: null
    };
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.chooseLab = this.chooseLab.bind(this);
    this.getUserLabs = this.getUserLabs.bind(this);
  }

  async getUserLabs (signal = null) {
    const { showError = () => ({}) } = this.props;
    try {
      const {
        userInfo: { token }
      } = this.state;
      const labs = await getUserLabsServ({ token }, signal);
      this.setState({ labs });
      return labs;
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(translateForUser(error));
      return null;
    }
  };

  login(uInfo) {
    const { userInfo, isAuthenticated } = _login({
      cookies: this.cookies,
      userInfo: uInfo,
    });
    this.setState({ userInfo, isAuthenticated });
    return { userInfo, isAuthenticated };
  }

  logout() {
    _logout({ cookies: this.cookies });
    this.setState({
      userInfo: null,
      isAuthenticated: false,
      labs: null,
      selectedLab: null
    });
  }

  chooseLab({ id }) {
    this.setState({ selectedLab: id });
  }

  render() {
    const { children } = this.props;
    const { userInfo, isAuthenticated, labs, selectedLab } = this.state;
    const authContext = {
      isAuthenticated,
      userInfo,
      selectedLab,
      labs,
      login: this.login,
      logout: this.logout,
      getUserLabs: this.getUserLabs,
      chooseLab: this.chooseLab,
    };
    return (
      <AuthContext.Provider value={authContext}>
        {children}
      </AuthContext.Provider>
    );
  }
}

export { AuthContext, useAuth, AuthConsumer };
export default AuthContext;

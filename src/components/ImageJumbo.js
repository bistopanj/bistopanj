import React from 'react';
import { Jumbotron } from 'reactstrap';

const imageContainerStyle = {
  height: 0,
  overflow: 'hidden',
  position: 'relative',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover'
};
const insideContentStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%'
};

const jumboStyle = {
  height: '100%',
  backgroundColor: 'transparent'
};

const ImageJumbo = props => (
  <div
    style={{
      ...imageContainerStyle,
      backgroundImage: `url(${props.background})`,
      paddingTop: props.ratio || '75%'
    }}
  >
    <div style={insideContentStyle}>
      <Jumbotron style={jumboStyle} {...props}>
        {props.children}
      </Jumbotron>
    </div>
  </div>
);

export default ImageJumbo;

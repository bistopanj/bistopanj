/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

const FullPageContainer = ({children, ...rest}) => (
  <main role="main">
    <div className="full-page-container" {...rest}>
      {children}
    </div>
  </main>
);

export default FullPageContainer;
import React from 'react';
import { Link } from 'react-router-dom';

const AppDownloadLink = ({
  icon = {},
  to = '/mobile-app/download',
  style = {}
}) => (
  <Link className="app-download-link" to={to} style={style} target="_blank">
    <img src={icon.src} srcSet={icon.srcSet} alt={icon.alt} />
  </Link>
);

export default AppDownloadLink;

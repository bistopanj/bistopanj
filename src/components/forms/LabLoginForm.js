import React, { Component } from 'react';
import LabGetOtpForm from './LabGetOtpForm';
import LabEnterOtpForm from './LabEnterOtpForm';

const pageModes = {
  getOtp: 1,
  enterOtp: 2,
};

class LabLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: null,
      mode: pageModes.getOtp,
    };
  }

  proceed = ({ mobileNumber }) => {
    this.setState({
      mode: pageModes.enterOtp,
      mobileNumber,
    });
  };

  restart = () => {
    this.setState({
      mode: pageModes.getOtp,
      mobileNumber: null,
    });
  };

  render() {
    const { mobileNumber, mode } = this.state;
    const { onLogin = () => ({}) } = this.props;
    return(
      mode === pageModes.getOtp ? (
        <LabGetOtpForm onFinish={this.proceed} />
      ) : (
        <LabEnterOtpForm onFinish={onLogin} mobileNumber={mobileNumber} goBack={this.restart} />
      )
    );
  }
} 

export default LabLoginForm;
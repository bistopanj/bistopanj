import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import CardForm from './CardForm';
import { useAppContext } from '../../contexts';

const minLabNameLength = 2;

// This object specifies the elements of the form in this page.
const formItems = [
  {
    name: 'labName',
    id: 'lab-name',
    placeholder: 'lab.register.lab-name-placeholder',
    description: 'lab.register.lab-name-description',
    type: 'text',
    autoFocus: true,
  },
];


const UserAgreement = () => {
  const { t } = useAppContext();
  return (
    <>
      {t('lab.register.you-accept-license-1')}
      <Link to="/lab/user-agreement" target="_blank" >
        {t('lab.register.you-accept-license-2')}
      </Link>
      {t('lab.register.you-accept-license-3')}
    </>
  );
};

const DeactivationItem = () => {
  const { t } = useAppContext();
  return (
    <>{t('lab.register.you-can-deactivate')}</>
  );
};

const belowItems = [
  <DeactivationItem />,
  <UserAgreement />
];


class LabRegisterForm extends Component {
  constructor (props) {
    super(props);
    this.form = null;
    this.state = {
      buttonActive: false,
      shouldRedirect: false,
    };
    this.listeners = { labName: this.handleLabNameChange };
  }

  handleLabNameChange = e => {
    const { buttonActive } = this.state;
    const labName = e.target.value || '';
    const labNameAcceptable = labName.length >= minLabNameLength;
    if((labNameAcceptable && !buttonActive) || (!labNameAcceptable && buttonActive)) {
      this.setState({ buttonActive: labNameAcceptable });
    }
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState({ shouldRedirect: true });
  };

  setFormRef = element => {
    this.form = element;
  };

  render() {
    const { buttonActive, shouldRedirect } = this.state;
    
    if(shouldRedirect) return(
      <Redirect
        to={{
          pathname: '/lab/create',
          state: { data: { labName: this.form.inputs.labName.value } },
        }}
      />
    );

    return (
      <CardForm
        buttonText="lab.register.set-it-up"
        formItems={formItems}
        cardTitle="lab.register.setup"
        listeners={this.listeners}
        belowButtonItems={belowItems}
        disabled={!buttonActive}
        onSubmit={this.onSubmit}
        ref={this.setFormRef}
      />
    );
  }
}

export default LabRegisterForm;
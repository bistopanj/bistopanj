import React, { Component } from 'react';
import CardForm from './CardForm';
import { parseMobileNumber } from '../../utils/tools';
import { getOtp } from '../../services/api.service';
import { translateForUser, errorTypes } from '../../errors';
import { AppContext } from '../../contexts';

const formItems = [
  {
    name: 'mobileNumber',
    label: 'lab.login.mobile-num-label',
    id: 'otp-mobile-number',
    placeholder: 'lab.login.mobile-num-placeholder',
    description: 'lab.login.mobile-num-desc',
    type: 'text',
    autoFocus: true
  }
];

const doNothing = () => ({});

class LabGetOtpForm extends Component {
  constructor(props) {
    super(props);
    this.form = null;
    this.abortController = new AbortController();
    this.state = {
      isFormReady: false,
      mobileNumber: null
    };
    this.listeners = {
      mobileNumber: this.handleMobileNumberChange
    };
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  handleMobileNumberChange = e => {
    const text = e.target.value;
    const mobileNumber = parseMobileNumber(text);
    if (mobileNumber) {
      this.setState({
        isFormReady: true,
        mobileNumber: mobileNumber.standardFormat
      });
    } else {
      this.setState({
        isFormReady: false,
        mobileNumber: null
      });
    }
  };

  setFormRef = element => {
    this.form = element;
  };

  onSubmit = e => {
    e.preventDefault();
    const { isFormReady } = this.state;
    if (!isFormReady) return false;
    this.requestOtp();
    return true;
  };

  requestOtp = async () => {
    const { showError = doNothing, t } = this.context;
    const { mobileNumber } = this.state;
    const { onFinish = doNothing } = this.props;
    const { wait = doNothing, endWait = doNothing } = this.form.button || {};
    try {
      wait();
      await getOtp({ mobileNumber }, this.abortController.signal);
      onFinish({ mobileNumber });
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  };

  render() {
    const { isFormReady } = this.state;
    return (
      <CardForm
        cardTitle="lab.login.get-otp-title"
        buttonText="lab.login.get-otp"
        formItems={formItems}
        listeners={this.listeners}
        disabled={!isFormReady}
        onSubmit={this.onSubmit}
        ref={this.setFormRef}
      />
    );
  }
}

LabGetOtpForm.contextType = AppContext;

export default LabGetOtpForm;

import React , { Component } from 'react';
import { Button } from 'reactstrap';
import CardForm from './CardForm';
import { convertNumeralsToRoman } from '../../utils/tools';
import { login } from '../../services/api.service';
import { appParams } from '../../constants';
import { translateForUser, errorTypes } from '../../errors';
import { AppContext, useAppContext } from '../../contexts';

const formItems = [
  {
    name: 'otp',
    id: 'otp',
    placeholder: 'lab.login.otp-placeholder',
    description: 'lab.login.otp-desc',
    type: 'text',
    autoFocus: true,
  },
];

const doNothing = () => ({});

const OtpRetry = ({ onRetry = doNothing }) => {
  const { t } = useAppContext();

  return (
    <>
      {t('lab.login.no-otp')}
      {' '}
      <Button color="link" onClick={onRetry}>
        {t('lab.login.new-otp')}
      </Button>
    </>
  );
};

class LabEnterOtpForm extends Component {
  constructor(props) {
    super(props);

    this.form = null;
    this.abortController = new AbortController();
    this.state = {
      isFormReady: false,
      otp: null,
    };

    this.listeners = {
      otp: this.handleOtpChange,
    };
    this.goBack = this.goBack.bind(this);
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  handleOtpChange = e => {
    const text = e.target.value;
    const converted = convertNumeralsToRoman(text);

    if(text.length >= appParams.minOtpLen){
      this.setState({
        isFormReady: true,
        otp: converted,
      });
    }
    else{
      this.setState({
        isFormReady: false,
        otp: null,
      });
    }
  };

  setFormRef = element => {
    this.form = element;
  };

  onSubmit = (e) => {
    e.preventDefault();

    const { isFormReady } = this.state;
    if(!isFormReady) return false;
    this.login();
    return true;
  };

  login = async () => {
    const { showError = doNothing, t } = this.context;
    const { otp } = this.state;
    const { mobileNumber, onFinish = () => ({}) } =  this.props;
    const { wait = doNothing, endWait = doNothing } = this.form.button || {};
    try {
      wait();
      const userInfo = await login({ mobileNumber, otp }, this.abortController.signal);
      onFinish({ userInfo });
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  };

  goBack() {
    const { goBack: _goBack = () => ({}) } = this.props;
    _goBack();
  };

  render() {
    const { isFormReady } = this.state;
    const belowButtonItems = [ <OtpRetry onRetry={this.goBack}/> ];

    return(
      <CardForm
        cardTitle="lab.login.enter-otp-title"
        buttonText="lab.login.enter"
        belowButtonItems={belowButtonItems}
        formItems={formItems}
        listeners={this.listeners}
        disabled={!isFormReady}
        onSubmit={this.onSubmit}
        ref={this.setFormRef}
      />
    );
  }
}

LabEnterOtpForm.contextType = AppContext;

export default LabEnterOtpForm;
import React, { Component } from 'react';
import {
  Card,
  CardTitle,
  CardHeader,
  CardBody,
  CardText,
  Form,
  FormGroup,
  Input,
  Label,
} from 'reactstrap';
import './CardForm.css';
import AsyncButton from '../AsyncButton';
import { useAppContext } from '../../contexts';

const titleStyle = {
  color: 'black',
  fontWeight: 'bold'
};
const bodyStyle = {
  padding: 0,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center'
};

const CardFormTitle = ({ title }) => {
  const { t } = useAppContext();
  return (
    <CardHeader className="text-center my-heading-font">
      <CardTitle style={titleStyle}>{t(title)}</CardTitle>
    </CardHeader>
  );
};

const FormItems = ({
  items = [],
  innerRef = () => ({}),
  onChange = () => ({})
}) => {
  const { t } = useAppContext();
  return (
    <div className="form-items-container">
      {items.map((item, index) => {
        const {
          description,
          label = null,
          id = null,
          autoFocus = false,
          type,
          name,
          placeholder = ''
        } = item;
        return (
          <FormGroup key={id || index}>
            {label && <Label for={id}>{t(label)}</Label>}
            <Input
              id={id}
              autoFocus={autoFocus}
              type={type}
              name={name}
              placeholder={t(placeholder)}
              innerRef={innerRef}
              onChange={onChange}
            />
            <CardText className="description-text" style={{ fontSize: '0.6rem'}}>{t(description)}</CardText>
          </FormGroup>
        );
      })}
    </div>
  );
};

const FormFooter = ({
  buttonText = 'submit',
  color = 'info',
  disabled = false,
  descItems = [],
  innerRef = () => ({}),
}) => {
  const { t } = useAppContext();
  return (
    <div className="form-footer-container">
      <AsyncButton block color={color} size="lg" disabled={disabled} ref={innerRef} >
        {t(buttonText)}
      </AsyncButton>
      <ul>
        { descItems.map((item = {}, key) => (
          <li key={ item.key || key}>
            <CardText className="below-button-text" style={{ fontSize: '0.6rem'}}>{item}</CardText>
          </li>
        ))}
      </ul>
    </div>
  );
};

class CardForm extends Component {
  constructor(props) {
    super(props);
    this.inputs = {};
    this.button = null;
    const { formItems = [] } = this.props;
    formItems.forEach(item => {
      if(item.name) this.inputs[item.name] = null;
    });
  }

  setInputRef = element => {
    const { name = null } = element || {};
    if(name) this.inputs[name] = element;
  };

  setButtonRef = element => {
    this.button = element;
  };

  handleChange = (e) => {
    const { listeners = {} } = this.props;
    const { name = null } = e.target;
    if(name in listeners) listeners[name](e);
  };

  render() {
    const {
      buttonText,
      formItems = [],
      cardTitle = 'Form',
      belowButtonItems = [],
      disabled = false,
      onSubmit = () => ({}),
    } = this.props;

    return (
      <Card className="form-card">
        <CardFormTitle title={cardTitle} />
        <CardBody className="text-justify" style={bodyStyle}>
          <Form onSubmit={onSubmit}>
            <FormItems
              items={formItems}
              onChange={this.handleChange}
              innerRef={this.setInputRef}
            />
            <FormFooter
              buttonText={buttonText}
              disabled={disabled}
              descItems={belowButtonItems}
              innerRef={this.setButtonRef}
            />
          </Form>
        </CardBody>
      </Card>
    );
  }
}

export default CardForm;

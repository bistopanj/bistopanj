import React from 'react';
import { Button, Spinner } from 'reactstrap';

class AsyncButton extends React.Component {
  constructor(props) {
    super(props);
    this.waitable = true;
    this.state = { waiting: false };
  }

  componentWillUnmount() {
    this.waitable = false;
  }

  wait = () => {
    if(this.waitable) this.setState({ waiting: true });
  };

  endWait = () => {
    if(this.waitable) this.setState({ waiting: false });
  };

  render() {
    const { waiting } = this.state;
    const { children, color = 'info', disabled, ...otherProps } = this.props;
    const buttonColor = waiting ? 'link' : color;

    return (
      <Button
        color={buttonColor}
        disabled={waiting || disabled}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...otherProps}
      >
        {waiting ? <Spinner color={color} size="sm" /> : children }
      </Button>
    );
  }
}

export default AsyncButton;
import React, { Component } from 'react';
import { Progress } from 'reactstrap';
import xhr from '../services/xhr.service';
import { translateForUser, errorTypes } from '../errors';
import { AppContext } from '../contexts';

const defaultLoadingText = 'lab.result.loading-image';
const defaultTextColor = 'rgba(255,255,255, 0.5)';

const progressContainerStyle = {
  width: '40vw',
  direction: 'ltr',
  display: 'flex',
  flexDirection: 'column'
};

const textStyle = {
  fontSize: '0.8rem',
  alignSelf: 'center'
};

const progressBarStyle = { borderRadius: 0 };

class ImageLoader extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    // this.xhr = null;
    this.blobUri = null;
    this.state = {
      loading: false,
      loaded: false,
      progress: 0,
      image: null
    };
    this.setImage = this.setImage.bind(this);
    this.reset = this.reset.bind(this);
    this.startProgress = this.startProgress.bind(this);
    this.updateProgress = this.updateProgress.bind(this);
    this.startLoading = this.startLoading.bind(this);
  }

  componentDidMount() {
    this.startLoading();
  }

  componentDidUpdate(prevProps) {
    const { src: oldSrc } = prevProps;
    const { src: newSrc } = this.props;
    const { loading } = this.state;

    if (oldSrc !== newSrc && !loading) {
      this.startLoading();
    }
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  setImage(blob) {
    const { onLoad = () => ({}) } = this.props;
    const theBlob = new Blob([blob]);
    const blobUri = window.URL.createObjectURL(theBlob);
    this.blobUri = blobUri;
    onLoad(blobUri);
    this.setState({ loaded: true, image: blobUri, loading: false });
  }

  reset() {
    this.setState({ loaded: false, progress: 0, image: null });
  }

  startProgress() {
    if (!this.abortController.signal.aborted) {
      this.setState({ progress: 0 });
    }
  }

  updateProgress(e) {
    if (!this.abortController.signal.aborted) {
      this.setState({ progress: parseInt((e.loaded / e.total) * 100, 10) });
    }
  }

  async startLoading() {
    const { src: url, onError } = this.props;
    const { showError, t } = this.context;
    this.setState({ loading: true, loaded: false });
    try {
      const imageBlob = await xhr(url, {
        signal: this.abortController.signal,
        onProgress: this.updateProgress,
        onStart: this.startProgress,
        responseType: 'blob'
      });
      this.setImage(imageBlob);
    } catch (error) {
      if (error.name !== errorTypes.AbortError) {
        showError(t(translateForUser(error)));
        onError(error);
      }
    }
  }

  render() {
    const { loaded, progress, image } = this.state;
    const {
      loadingText,
      textColor = defaultTextColor,
      alt = 'image'
    } = this.props;
    const { t } = this.context;
    if (loaded) return <img src={image} alt={alt} />;
    // return (
    //   <Button color="link" onClick={onClick} className="m-0 p-0">
    //     <img src={image} alt={alt} />
    //   </Button>
    // );

    return (
      <div style={progressContainerStyle}>
        <p style={{ ...textStyle, color: textColor }}>
          {t(loadingText || defaultLoadingText)}
        </p>
        <Progress value={progress} color="info" style={progressBarStyle} />
      </div>
    );
  }
}

ImageLoader.contextType = AppContext;

export default ImageLoader;

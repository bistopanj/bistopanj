import React from 'react';
import { matchPath, Link, withRouter } from 'react-router-dom';
import { Container, Jumbotron, Row, Col } from 'reactstrap';
import './Footer.css';
import { useAppContext } from '../contexts';
import { products } from '../data/products';

const quickLinks = [
  { name: 'company.footer.ql-company', href: '/', key: 'company' },
  { name: 'company.footer.ql-lab', href: '/lab', key: 'lab' },
  {
    name: 'company.footer.ql-download-ios',
    href: '/lab/mobile-app/ios',
    target: '_blank',
    key: 'ios-download',
  },
  {
    name: 'company.footer.ql-download-cb',
    href: '/lab/mobile-app/android/cafebazaar',
    target: '_blank',
    key: 'cafebazaar-download',
  },
  {
    name: 'company.footer.ql-download-gp',
    href: '/lab/mobile-app/android/googleplay',
    target: '_blank',
    key: 'googleplay-download',
  },
  { name: 'company.footer.ql-lab-signin', href: '/lab/dashboard', key: 'lab-signin' },
  { name: 'company.footer.ql-contacts', href: '/contacts', key: 'contacts' },
  { name: 'company.footer.lab-user-agreement', href: '/lab/user-agreement', key: 'lab-user-agreement' },
  { name: 'company.footer.ql-privacy', href: '/privacy', key: 'privacy' }
];

const Footer = ({ exclude = null, location = {} }) => {
  const { t } = useAppContext();
  if (exclude) {
    // If this path is excluded, return null to render nothing as navbar. This path will have its own navbar.
    const mustExclude = matchPath(location.pathname, {
      path: exclude,
      exact: false,
      strict: false
    });
    if (mustExclude) return null;
  }
  return (
    <footer id="site-footer" className="py-4" >
      <Jumbotron fluid className="bg-transparent my-0 text-justify">
        <Container>
          <Row>
            <Col className="footer-col" md={5}>
              <h6 className="my-heading-font">{t('company.footer.about-us-title')}</h6>
              <p>{t('company.footer.about-us')}</p>
            </Col>
            <Col className="footer-col" md={3}>
              <h6 className="my-heading-font">{t('company.footer.products-title')}</h6>
              {products.map((prod) => (
                <Link to={prod.href} key={`product-link-${prod.key}`}>
                  {`${t(prod.name)} ${t('general.for')}${t(prod.for)}`}
                </Link>
              ))}
            </Col>
            <Col className="footer-col" md={4}>
              <h6 className="my-heading-font">{t('company.footer.quick-access-title')}</h6>
              {quickLinks.map(({ key, href, target, name }) => (
                <Link
                  to={href}
                  key={`quick-access-${key}`}
                  target={target || undefined}
                >
                  {t(name)}
                </Link>
              ))}
            </Col>
          </Row>
          <hr />
          <p className="copyright">{t('company.footer.copyright')}</p>
        </Container>
      </Jumbotron>
    </footer>
  );
};

export default withRouter(Footer);

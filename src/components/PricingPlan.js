import React from 'react';
import { Card, CardText, CardHeader, CardBody, CardTitle } from 'reactstrap';
import { Link } from 'react-router-dom';
import './PricingPlan.css';
import { useAppContext } from '../contexts';

const PricingPlan = ({
  title = '',
  description = '',
  sendCredit = '',
  price = '',
  usersPerLab = '',
  buttonText = '',
  buttonHref = '/',
  borderColor = 'black',
  belowButton = null
}) => {
  const { t } = useAppContext();
  return(
    <Card className="pricing-plan-card" style={{ borderColor }}>
      <CardHeader style={{ borderColor }}>
        <CardTitle className="my-heading-font h5 mb-5">{t(title)}</CardTitle>
        <CardText>{t(description)}</CardText>
        <h5 className="my-heading-font">
          {t(price)}
          {' '}
          {t('general.currency')}
          {' '}
          <span className="price-desc">{t('lab.landing.pricing-per-send')}</span>
        </h5>
        <hr />
      </CardHeader>
      <CardBody>
        <CardText>{`${t('lab.landing.pricing-send-limit')} ${t(sendCredit)}`}</CardText>
        <CardText>{`${t('lab.landing.pricing-user-limit')} ${t(usersPerLab)}`}</CardText>
        <Link to={buttonHref} className="btn btn-info btn-md btn-block btn-lg">
          {t(buttonText)}
        </Link>
        {belowButton && (
          <CardText className="small-text below-button-text" style={{ fontSize: '0.6rem'}}>{t(belowButton)}</CardText>
        )}
      </CardBody>
    </Card>
  );
};

export default PricingPlan;

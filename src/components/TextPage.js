import React from 'react';
import { Container } from 'reactstrap';
import './TextPage.css';


const TextPage = ({ children }) => (
  <main role="main">
    <Container className="text-page-container">
      {children}
    </Container>
  </main>
);

export default TextPage;
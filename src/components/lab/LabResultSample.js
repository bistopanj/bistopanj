import React from 'react';
import { Link } from 'react-router-dom';
import './LabResultSample.css';
import { useAppContext } from '../../contexts';
import { sampleResults } from '../../constants';

const LabResultSample = () => {
  const { t, i18n } = useAppContext();
  const { language = 'fa' } = i18n;

  return (
    <div id="lab-result-poster">
      <div>
        <div className="poster-message">
          <h3 className="my-heading-font">{t('lab.landing.sample-title')}</h3>
          <p>{t('lab.landing.sample-subtitle-1')}</p>
          <p>{t('lab.landing.sample-subtitle-2')}</p>
          <Link
            className="btn btn-outline-secondary btn-lg my-button-white"
            to={`/lab/result/${sampleResults[language]}`}
            target="_blank"
          >
            {t('lab.landing.see-sample')}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LabResultSample;

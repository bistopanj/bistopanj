import React from 'react';
import { Row, Col, Container, Jumbotron } from 'reactstrap';
import './LabDownload.css';
import { useAppContext } from '../../contexts';
import AppDownloadLink from '../AppDownloadLink';
import {
  downloadAppAS,
  downloadAppCB,
  downloadAppGP,
  labScreenShot2
} from '../../images';

const steps = [
  'lab.landing.sending-results-step-1',
  'lab.landing.sending-results-step-2',
  'lab.landing.sending-results-step-3'
];
const LabDownload = () => {
  const { t, i18n } = useAppContext();
  const { language = 'fa' } = i18n;
  return (
    <Jumbotron fluid id="lab-download" className="text-center">
      <Container>
        <h3 className="my-heading-font">{t('lab.landing.mobile-app')}</h3>
        <Row>
          <Col className="description-col" md="6">
            <h5 className="my-heading-font">
              {t('lab.landing.sending-results')}
            </h5>
            <p>{t('lab.landing.sending-results-desc')}</p>
            <ol>
              {steps.map(step => (
                <li key={step}>{t(step)}</li>
              ))}
            </ol>
            <div>
              <AppDownloadLink
                icon={downloadAppCB}
                to="/lab/mobile-app/android/cafebazaar"
              />
              <AppDownloadLink
                icon={downloadAppGP.langs[language]}
                to="/lab/mobile-app/android/googleplay"
              />
              <AppDownloadLink
                icon={downloadAppAS.langs[language]}
                to="/lab/mobile-app/ios"
              />
            </div>
          </Col>
          <Col className="screenshot-col p-4" md="6">
            <img
              src={labScreenShot2.langs[language].src}
              srcSet={labScreenShot2.langs[language].srcSet}
              alt={labScreenShot2.langs[language].alt}
            />
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  );
};

export default LabDownload;

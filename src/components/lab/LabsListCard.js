import React from 'react';
import { Card, CardHeader, CardTitle, CardBody, CardFooter, CardText } from 'reactstrap';
import '../forms/CardForm.css';
import LabsListItem from './LabsListItem';
import { useAppContext } from '../../contexts';

const bodyStyle = {
  padding: 0,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
};

const LabsListCard = ({ labs = [], onSelect=() => ({}) }) => {
  const { t } = useAppContext();
  return (
    <Card className="form-card">
      <CardHeader className="text-justify p-0">
        <CardTitle>
          {t('lab.register.choose-lab')}
        </CardTitle>
      </CardHeader>
      <CardBody style={bodyStyle}>
        {
          labs.map((lab, index) => (
            <LabsListItem lab={lab} key={lab._id || index} onSelect={onSelect}/>
            )
          )
        }
      </CardBody>
      <CardFooter>
        <CardText className="below-button-text" style={{ fontSize: '0.6rem' }}>{t('lab.register.you-can-change')}</CardText>
      </CardFooter>
    </Card>
  );
};

export default LabsListCard;
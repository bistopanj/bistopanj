import React from 'react';
import { Link } from 'react-router-dom';
import './LabPoster.css';
import { useAppContext } from '../../contexts';

const LimitdPoster = () => {
  const { t, i18n } = useAppContext();
  const { language = 'fa' } = i18n;
  return (
    <div id="lab-value-poster" className={`poster-${language}`}>
      <div>
        <div className="poster-message">
          <h1 className="my-heading-font">{t('lab.landing.poster-title')}</h1>
          <p>{t('lab.landing.poster-subtitle')}</p>
          <Link
            className="poster-button-white btn btn-outline-secondary btn-lg"
            to="/lab/register-limited"
          >
            {t('lab.landing.setup')}
            {' *'}
          </Link>
          <p className="limited-notice">
            {'* '}
            {t('lab.landing.limited')}
          </p>
        </div>
      </div>
    </div>
  );
};

const LabPoster = ({ limited = false }) => {
  const { t, i18n } = useAppContext();
  const { language = 'fa' } = i18n;
  if (limited) return <LimitdPoster />;
  return (
    <div id="lab-value-poster" className={`poster-${language}`}>
      <div>
        <div className="poster-message">
          <h1 className="my-heading-font">{t('lab.landing.poster-title')}</h1>
          <p>{t('lab.landing.poster-subtitle')}</p>
          <Link
            className="poster-button-white btn btn-outline-secondary btn-lg"
            to="/lab/register"
          >
            {t('lab.landing.setup')}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LabPoster;

import React from 'react';
import { Helmet } from 'react-helmet';
import { useAppContext } from '../../contexts';

const LabHeadTags = () => {
  const { t } = useAppContext();
  return (
    <Helmet>
      <title> {t('tags.title-lab')} </title>
      <meta
        name="description"
        content={t('tags.description-lab')}
      />
      {/* <link rel="shortcut icon" href="/static/media/lab-favicon.ico" /> */}
      <link rel="shortcut icon" href="/lab-favicon.ico" />
    </Helmet>
  );
};

export default LabHeadTags;
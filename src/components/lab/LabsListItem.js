import React from 'react';
import './LabsListItem.css';

const LabsListItem = ({
  lab,
  onSelect = () => ({}),
}) => (
  <button
    type="button"
    className="labs-list-item"
    color="info"
    onClick={() => onSelect({id: lab._id})}
  >
    {lab.name}
  </button>
);

export default LabsListItem;

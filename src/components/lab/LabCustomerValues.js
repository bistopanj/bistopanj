import React from 'react';
import JumboMultiCol from '../JumboMultiCol';
import content from '../../contents/LabCustomerValues';

const LabCustomerValues = props => (
  <JumboMultiCol
    id="lab-customer-values"
    content={content}
    logoColor="#990099"
    textColor="#989898"
    jumboTitleColor="#990099"
    backgroundColor="white"
    itemTitleColor="black"
    cols={4}
  />
);

export default LabCustomerValues;

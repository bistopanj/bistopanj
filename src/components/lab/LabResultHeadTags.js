import React from 'react';
import { Helmet } from 'react-helmet';
import { useAppContext } from '../../contexts';

const LabResultHeadTags = () => {
  const { t } = useAppContext();
  return (
    <Helmet>
      <title> {t('tags.title-result-ready')} </title>
      <link rel="shortcut icon" href="/lab-favicon.ico" />
    </Helmet>
  );
};

export default LabResultHeadTags;
import React from 'react';
import { Button } from 'reactstrap';
import './LabResultActions.css';
import Logo from '../Logo';
import { useAppContext } from '../../contexts';

const Icon = ({
  name,
  color,
  size,
  text = null,
  href = null,
  download = false,
  onClick = () => ({}),
}) => (
  <Button color="link" onClick={onClick} href={href} download={download}>
    <Logo name={name} color={color} size={size} />
    {text && <p>{text}</p>}
  </Button>
);

const iconSize = 30;

const doNothing = () => ({});

const LabResultActions = ({ actions = {}, image = null, showHQ = true }) => {
  const { t } = useAppContext();
  return (
    <div className="bottom-action-center">
      <Icon
        name="save"
        size={iconSize}
        text={t('lab.result.save-image')}
        href={image}
        download="test_result.jpg"
        onClick={actions.saveImage || doNothing}
      />
      { showHQ && (
        <Icon
          name="zoom"
          size={iconSize}
          text={t('lab.result.get-high-quality')}
          onClick={actions.getLargeImage || doNothing}
        />
      )}
    </div>
  );
};

export default LabResultActions;

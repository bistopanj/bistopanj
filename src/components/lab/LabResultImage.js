import React, { useState } from 'react';
import { Button } from 'reactstrap';
import './LabResultImage.css';
import ImageLoader from '../ImageLoader';
import Logo from '../Logo';
import { useAppContext } from '../../contexts';
import { polishNumber, getMobileOS } from '../../utils/tools';
import { getHqImage, getEcoImage } from '../../services/image.service';

const iconSize = 20;
const doNothing = () => ({});

const Icon = ({
  name,
  color,
  size,
  text = null,
  href = null,
  download = false,
  onClick = () => ({})
}) => (
  <Button color="link" onClick={onClick} href={href} download={download}>
    <Logo name={name} color={color} size={size} />
    {text && <p>{text}</p>}
  </Button>
);

const ImageActions = ({
  image,
  imageUrl,
  loadHq = doNothing,
  hq = false,
  index = null
}) => {
  const { t } = useAppContext();
  const os = getMobileOS();
  const link = os === 'iOS' ? imageUrl : image;
  return (
    <div className="action-center">
      <Icon
        name="save"
        size={iconSize}
        text={t('lab.result.save-image')}
        href={link}
        download={`test_result_${index + 1}.jpg`}
        onClick={doNothing}
      />
      {!hq && (
        <Icon
          name="zoom"
          size={iconSize}
          text={t('lab.result.get-high-quality')}
          onClick={loadHq}
        />
      )}
    </div>
  );
};

const ImageHeader = ({
  index = null,
  image,
  imageUrl,
  loadHq = doNothing,
  hq = false
}) => {
  const { t, i18n } = useAppContext();
  const pageTitle = `${t('general.page')} ${polishNumber(
    `${index + 1}`,
    i18n.language
  )}`;
  return (
    <div className="image-header">
      <div className="my-auto">
        <p className="my-auto text-white">{pageTitle}</p>
      </div>
      <ImageActions
        image={image}
        imageUrl={imageUrl}
        loadHq={loadHq}
        hq={hq}
        index={index}
      />
    </div>
  );
};

const LabResultImage = ({ photo, index = null }) => {
  const [hq, setHq] = useState(false);
  const [imageUrl, setImageUrl] = useState(getEcoImage(photo));
  const [image, setImage] = useState(null);

  const loadHqImage = () => {
    setHq(true);
    setImageUrl(getHqImage(photo));
  };

  const handleLoad = img => setImage(img);

  return (
    <>
      <ImageHeader
        index={index}
        image={image}
        imageUrl={imageUrl}
        loadHq={loadHqImage}
        hq={hq}
      />
      <ImageLoader src={imageUrl} alt="test result" onLoad={handleLoad} />
    </>
  );
};

export default LabResultImage;

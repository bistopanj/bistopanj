import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import MyGrid from '../MyGrid';
import PricingPlan from '../PricingPlan';
import content from '../../contents/LabPricing';
import { useAppContext } from '../../contexts';

const gen = (cont, key) => {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <PricingPlan borderColor="#E5E8E8" {...cont} key={key} />;
};

const LabPricing = () => {
  const { t } = useAppContext();
  return (
    <Jumbotron
      id="pricing-plan"
      fluid
      className="my-0 text-center"
      style={{ backgroundColor: '#f8f9fa' }}
    >
      <Container>
        <h3 className="my-heading-font" style={{ color: '#980098' }}>
          {t('lab.landing.pricing-title')}
        </h3>
        <MyGrid
          contents={content.plans}
          cols={2}
          genItem={gen}
          colClassName="p-3"
        />
      </Container>
    </Jumbotron>
  );
};

export default LabPricing;

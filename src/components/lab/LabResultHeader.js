import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';
// import './LabResultHeader.css';

const descriptionStyle = {
  backgroundColor: '#eaeded',
  margin: 0,
};

const labDescriptionStyle = { fontSize: '0.7rem', margin: 0, padding: '0.5rem' };
const LabResultHeader = ({ labName, labDescription = null }) => {
  return (
    <>
      <header>
        <Navbar className="d-flex justify-content-center text-black bg-white">
          <NavbarBrand>
            {labName}
          </NavbarBrand>
        </Navbar>
      </header>
      { labDescription ? (
        <div className="text-center text-black-50" style={descriptionStyle}>
          <p style={labDescriptionStyle}>{labDescription}</p>
        </div>
      ) : null
      }
    </>
  );
};

export default LabResultHeader;
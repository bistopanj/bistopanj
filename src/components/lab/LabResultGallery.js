import React from 'react';
import './LabResultGallery.css';
import LabResultImage from './LabResultImage';

const LabResultGallery = ({ photos = [] }) => {
  // const [zoomed, setZoomed] = useState(false);

  // const toggleZoom = () => setZoomed(oldState => !oldState);
  const toggleZoom = () => {};

  return (
    <div className="gallery-placeholder normal">
      {photos.map((photo, index) => (
        <LabResultImage
          key={photo}
          photo={photo}
          index={index}
          toggleZoom={toggleZoom}
        />
      ))}
    </div>
  );
};

export default LabResultGallery;

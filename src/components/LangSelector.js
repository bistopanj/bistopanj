import React from 'react';
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import { useTranslation } from 'react-i18next';
import { langs } from '../constants';
import { useAppContext } from '../contexts';

const findLang = lng => {
  if (lng in langs) return langs[lng];
  return langs.en;
};

const langItemStyle = { textAlign: 'justify' };

const LangSelector = ({
  nav = false,
  inNavbar = false,
}) => {
  const { i18n } = useTranslation();
  const { direction } = useAppContext();
  const currentLang = findLang(i18n.language);
  const menuStyle =
    direction === 'ltr'
      ? { left: 'auto', right: 0 }
      : { left: 0, right: 'auto' };

  const setLang = langKey => i18n.changeLanguage(langKey);

  return (
    <UncontrolledDropdown nav={nav} inNavbar={inNavbar} >
      <DropdownToggle nav caret>
        {currentLang.title}
      </DropdownToggle>
      <DropdownMenu style={menuStyle} >
        {Object.keys(langs).map(key => (
          <DropdownItem
            key={`lang-selector-item-${key}`}
            onClick={() => setLang(key)}
            className={langs[key].dir}
            style={langItemStyle}
          >
            {langs[key].title}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default LangSelector;

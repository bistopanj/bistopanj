import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import './SharedModal.css';

const defaultConfirmText = 'confirm';
const defaultCancelText = 'cancel';
const doNothing = () => ({});

class SharedModal extends Component {
  constructor(props) {
    super(props);
    this.onConfirm = doNothing;
    this.onClose = doNothing;
    this.state = {
      isOpen: false,
      confirmText: null,
      cancelText: null,
      body: null,
      title: null,
      noCancel: false
    };
  }

  open = ({
    title = null,
    body = null,
    confirmText = defaultConfirmText,
    cancelText = defaultCancelText,
    onConfirm = doNothing,
    onCancel = doNothing,
    noCancel = false
  }) => {
    this.onConfirm = onConfirm || doNothing;
    this.onClose = onCancel || doNothing;
    this.setState({
      isOpen: true,
      title,
      body,
      confirmText,
      cancelText,
      noCancel
    });
  };

  handleConfirm = () => {
    this.onConfirm();
    this.close();
  };

  handleCancel = () => {
    this.onClose();
    this.close();
  };

  close = () => {
    this.setState({ isOpen: false });
  };

  render() {
    const {
      isOpen,
      confirmText,
      cancelText,
      title,
      body,
      noCancel
    } = this.state;
    const { direction } = this.props;
    return (
      <Modal isOpen={isOpen} className={`shared-modal ${direction}`}>
        {title && <ModalHeader>{title}</ModalHeader>}
        {body && <ModalBody>{body}</ModalBody>}
        <ModalFooter className="button-placeholder">
          {!noCancel && (
            <Button outline color="secondary" onClick={this.handleCancel}>
              {cancelText || defaultCancelText}
            </Button>
          )}
          <Button color="info" onClick={this.handleConfirm}>
            {confirmText || defaultConfirmText}
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default SharedModal;

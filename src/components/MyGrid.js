import React from 'react';
import { Row, Col } from 'reactstrap';

const MyGrid = props => {
  const {
    style = {},
    contents = [],
    cols = 1,
    genItem,
    colClassName = ''
  } = props;

  const numRow = Math.ceil(contents.length / cols);
  const colLength = Math.floor(12 / cols);
  const rows = [...Array(numRow).keys()];
  const columns = [...Array(cols).keys()];

  return (
    <div style={style}>
      {rows.map(row => (
        <Row key={row}>
          {columns.map(col => {
            const index = row * cols + col;
            return (
              <Col className={colClassName} md={colLength} key={col}>
                {index < contents.length ? genItem(contents[index], col) : null}
              </Col>
            );
          })}
        </Row>
      ))}
    </div>
  );
};

export default MyGrid;

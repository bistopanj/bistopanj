import React, { Component } from 'react';
import { Container, Button } from 'reactstrap';
import './Toasts.css';

import constants from '../constants';

const { defaultToastDuration } = constants.appParams;

const doNothing = () => ({});


class Toast extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.close = this.close.bind(this);
  }

  componentDidMount() {
    const { duration = defaultToastDuration } = this.props;
    this.timer = setTimeout(this.close, duration);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  close() {
    const { id, onDismiss = doNothing } = this.props;
    onDismiss(id);
  }

  render() {
    const {
      content,
      color = 'primary',
      direction = 'inherit',
    } = this.props;

    const containerStyle = {
      direction,
      backgroundColor: `var(--${color})`,
    };

    return (
      <>
        <div className="toast-notif" style={containerStyle}>
          <div className="toast-content"> {content}</div>
          <Button
            className="toast-button"
            close
            aria-label="close"
            onClick={this.close}
          />
        </div>
      </>
    );
  }
}

const Toasts = ({ toasts, onDismiss }) => (
  <div id="toasts-container">
    <Container>
      {toasts.map(({ id, content, options }) => (
        <Toast
          key={id}
          id={id}
          content={content}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...options}
          onDismiss={onDismiss}
        />
      ))}
    </Container>
  </div>
);

export default Toasts;

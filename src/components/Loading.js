import React from 'react';
import { Spinner } from 'reactstrap';
import FullPageContainer from './FullPageContainer';


const Loading = () => (
  <FullPageContainer>
    <Spinner size="lg" color="info" />
  </FullPageContainer>
);

export default Loading;
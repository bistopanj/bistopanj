import React, { Component } from 'react';
import './PageWithSidebar.css';
import { AppContext } from '../contexts';

const breakpointWidth = 768;

const pickStyle = ({
  direction = 'rtl',
  sidebarWidth = 250,
  sidebarActive = true
}) => {
  let sidebarStyle;
  let contentStyle;

  if (direction === 'rtl') {
    sidebarStyle = {
      width: sidebarWidth,
      marginRight: sidebarActive ? 0 : `-${sidebarWidth}`
    };
    contentStyle = {
      paddingRight: sidebarActive ? sidebarWidth : 0
    };
  } else {
    sidebarStyle = {
      width: sidebarWidth,
      marginLeft: sidebarActive ? 0 : `-${sidebarWidth}`
    };
    contentStyle = {
      paddingLeft: sidebarActive ? sidebarWidth : 0
    };
  }

  return { sidebarStyle, contentStyle };
};

class PageWithSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarActive: window.innerWidth > breakpointWidth,
      showDefault: window.innerWidth > breakpointWidth
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    const { showDefault } = this.state;
    const showDefaultNew = window.innerWidth > breakpointWidth;
    if (showDefaultNew !== showDefault)
      this.setState({
        showDefault: showDefaultNew,
        sidebarActive: showDefaultNew
      });
  };

  toggle = () => {
    this.setState(state => ({ sidebarActive: !state.sidebarActive }));
  };

  setSidebarStatus = (status = true) => {
    this.setState({ sidebarActive: status });
  };

  onItemSelect = () => {
    const { showDefault } = this.state;
    this.setSidebarStatus(showDefault);
  };

  render() {
    const { sidebarActive } = this.state;
    const { direction } = this.context;
    const {
      sidebar = null,
      children = null,
      sidebarWidth = '250px'
    } = this.props;
    const { sidebarStyle, contentStyle } = pickStyle({
      direction,
      sidebarWidth,
      sidebarActive
    });

    return (
      <div className="shrinkable-page-wrapper">
        <div
          className={`shrinkable-sidebar sidebar-${direction}`}
          style={sidebarStyle}
        >
          {sidebar}
        </div>
        <div className="shrinkable-content" style={contentStyle}>
          {children}
        </div>
      </div>
    );
  }
}

PageWithSidebar.contextType = AppContext;

export default PageWithSidebar;

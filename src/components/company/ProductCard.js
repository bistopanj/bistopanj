import React from 'react';
import {
  Card,
  CardBody,
  CardText,
  CardHeader,
  CardImg,
  CardTitle,
  CardFooter
} from 'reactstrap';
import { Link } from 'react-router-dom';
import './ProductCard.css';
import { useAppContext } from '../../contexts';


const cardStyle = { maxWidth: 300, minWidth: 250 };
const learnMore = 'company.home.learn-more';
const cardTextStyle = {
  textAlign: 'justify',
  lineHeight: '1.7'
};

const ProductLink = ({ to = null, href = null, children }) => {
  if(to)
    return (
      <Link className="btn btn-info btn-block btn-lg" to={to}>
        {children}
      </Link>
    );
  return (
    <a className="btn btn-info btn-block btn-lg" href={href}>
      {children}
    </a>
  );
};

const buildSrcSet = set =>
  Object.keys(set)
    .map(key => `${set[key]} ${key}`)
    .join(',');

const ProductCard = ({ product = {}, logo = {}}) => {
  const { t } = useAppContext();
  return (
    <Card className="product-card text-center my-2" style={cardStyle}>
      <CardHeader className="my-heading-font">{`${t('general.for')}${t(product.for)}`}</CardHeader>
      <CardBody>
        <CardImg
          src={logo.src}
          srcSet={buildSrcSet(logo.set)}
          alt={logo.alt || ''}
        />
        <CardTitle>
          {t(product.name)}
        </CardTitle>
        <CardText style={cardTextStyle}>{t(product.description)}</CardText>
      </CardBody>
      <CardFooter>
        <ProductLink to={product.to} href={product.href} >
          {t(product.buttonText) || t(learnMore)}
        </ProductLink>
      </CardFooter>
    </Card>
  );
};

export default ProductCard;

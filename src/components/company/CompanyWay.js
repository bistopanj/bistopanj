import React from 'react';
import JumboMultiCol from '../JumboMultiCol';
import content from '../../contents/CompanyWay';

const CompanyWay = props => (
  <JumboMultiCol
    id="company-way"
    content={content}
    logoColor="#1f28de"
    textColor="#989898"
    jumboTitleColor="#1f28de"
    backgroundColor="white"
    itemTitleColor="black"
    cols={2}
  />
);

export default CompanyWay;

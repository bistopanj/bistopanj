import React from 'react';
import { Container, Jumbotron } from 'reactstrap';
import './CompanyProducts.css';
import ProductCard from './ProductCard';
import { productsWithSuggestion as products } from '../../data/products';
import { useAppContext } from '../../contexts';

const CompanyProducts = () => {
  const { t } = useAppContext();
  return (
    <Jumbotron id="company-products" fluid>
      <Container>
        <p className="larger-p">{t('company.home.our-job')}</p>
        <div className="card-container">
          {products.map(prod => (
            <ProductCard key={`product-car-${prod.key}`} product={prod} logo={prod.logo} />
          ))}
        </div>
      </Container>
    </Jumbotron>
  );
};

export default CompanyProducts;
import React from 'react';
import { Helmet } from 'react-helmet';
import { useAppContext } from '../../contexts';

const DefaultHeadTags = () => {
  const { t } = useAppContext();
  return (
    <Helmet>
      <title> {t('tags.title-default')} </title>
      <meta
        name="description"
        content={t('tags.description-default')}
      />
      {/* <link rel="shortcut icon" href="/static/media/favicon.ico" /> */}
      <link rel="shortcut icon" href="/favicon.ico" />
    </Helmet>
  );
};

export default DefaultHeadTags;
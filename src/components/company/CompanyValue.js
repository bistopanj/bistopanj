import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import { useAppContext } from '../../contexts';

const jumboStyle = { minHeight: '70vh' };

const CompanyValue = () => {
  const { t } = useAppContext();
  return (
    <Jumbotron
      fluid
      className="text-info my-0 bg-white text-center d-flex justify-content-center align-items-center"
      style={jumboStyle}
    >
      <Container>
        <h2 className="my-heading-font">
          {t('company.home.mission-start')}
          <span className="" style={{ color: 'orangered' }}>
            {t('company.home.mission-emph')}
          </span>
          {t('company.home.mission-end')}
        </h2>
      </Container>
    </Jumbotron>
  );
};

export default CompanyValue;

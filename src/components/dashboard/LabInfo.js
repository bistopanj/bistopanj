import React, { Component } from 'react';
import { Button } from 'reactstrap';
import './LabInfo.css';
import { AuthContext, useAppContext } from '../../contexts';
import LabInfoForm from './LabInfoForm';

const fieldsToShow = {
  name: 'lab.dashboard.lab-name',
  description: 'lab.dashboard.lab-desc',
  logoUrl: 'lab.dashboard.lab-logo',
};

const pageModes = {
  show: true,
  edit: false,
};

const EditInfoButton = ({ onClick = () => ({})}) => {
  const { t } = useAppContext();
  return (
    <td rowSpan={0}>
      <Button color="link" onClick={onClick}>{t('lab.dashboard.edit-info')}</Button>
    </td>
  );
};

const LabInfoTable = ({ lab, onEdit = () => ({}) }) => {
  const { t } = useAppContext();
  return (
    <table>
      <tbody>
        {
          Object.keys(fieldsToShow).map( (key, i) => (
            key in lab ? (
              <tr key={key}>
                <td>
                  {t(fieldsToShow[key])}
                </td>
                <td>
                  {lab[key]}
                </td>
                { i === 0 && (
                  <EditInfoButton onClick={ onEdit } />
                )}
              </tr>
            ) : null
          ))
        }
      </tbody>
    </table>
  );
};

class LabInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: pageModes.show
    };
  }

  toggleMode = () => {
    this.setState( prevState => ({ mode: !prevState.mode }));
  };

  render() {
    const { lab } = this.props;
    const { mode } = this.state;
    return (
      <div className="lab-info-container">
        {mode === pageModes.show ? <LabInfoTable lab={lab} onEdit={this.toggleMode} /> : <LabInfoForm lab={lab} onFinish={this.toggleMode}/>}
      </div>
    );
  }
}

LabInfo.contextType = AuthContext;

export default LabInfo;

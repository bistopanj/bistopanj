import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

const FormItem = ({
  type = 'text',
  id,
  name,
  placeholder = '',
  label = null,
  required = false,
  defaultValue = '',
  innerRef = () => ({}),
  onChange = () => ({}),
}) => (
  <FormGroup>
    <Label for={id}>{label || name}</Label>
    <Input
      type={type}
      id={id}
      name={name}
      placeholder={placeholder}
      required={required}
      onChange={onChange}
      innerRef={innerRef}
      defaultValue={defaultValue}
    />
  </FormGroup>
);

export default FormItem;
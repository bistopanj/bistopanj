import React from 'react';
import Sidebar from '../Sidebar';
import { lab as labLogo } from '../../contents/logos';
import items from '../../data/dashboard-menu-items';
import LabCredit from './LabCredit';

const DashboardSidebar = ({
  labs = [],
  selectedLab = null,
  onItemSelect = () => ({})
}) => {
  const theLab = labs.find(lab => lab._id === selectedLab);
  return (
    <Sidebar
      lang
      items={items}
      logo={labLogo}
      headerText={theLab.name}
      headerSubtextComponent={<LabCredit lab={theLab} />}
      onItemSelect={onItemSelect}
    />
  );
};

export default DashboardSidebar;

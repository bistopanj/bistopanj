import React from 'react';
import { Navbar, NavbarToggler } from 'reactstrap';
import { Link } from 'react-router-dom';
import './DashboardNavbar.css';
import Logo from '../Logo';

const DashboardNavbar = ({ onToggle = () => ({})}) => (
  <header>
    <Navbar className="dashboard-navbar">
      <NavbarToggler onClick={onToggle} className="border-0 custom-toggler">
        <span className="navbar-toggler-icon black-purple" />
      </NavbarToggler>
      <Link to="/lab/dashboard">
        <Logo name="home" size={30}/>
      </Link>
    </Navbar>
  </header>
);

export default DashboardNavbar;
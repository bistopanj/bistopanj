import React, { Component } from 'react';
import { Table, Spinner } from 'reactstrap';
import './LabPersonnelTable.css';
import { getLabUser, removeLabUser } from '../../services/api.service';
import Logo from '../Logo';
import AsyncButton from '../AsyncButton';
import { polishNumber } from '../../utils/tools';
import { AppContext, useAppContext } from '../../contexts';
import { translateForUser, errorTypes } from '../../errors';

const tableHeaders = ['#', 'general.name', 'general.surname', 'general.mobile-number'];
const doNothing = () => ({});

const RemoveUserButton = ({
  onClick,
  disabled = false,
  innerRef = () => ({})
}) =>
  !disabled && (
    <AsyncButton
      close
      aria-label="Remove-User"
      onClick={onClick}
      color="danger"
      ref={innerRef}
    >
      <Logo name="times" size={20} color="red" />
    </AsyncButton>
  );

const TableSpinner = () => (
  <tr>
    <td align="center" colSpan="4">
      <Spinner size="sm" color="dark" />
    </td>
  </tr>
);

const TableHeader = ({ headers }) => {
  const { t } = useAppContext();
  return (
    <thead className="thead-dark">
      <tr>
        <th className="width-min" aria-label="th"/>
        <th>{t(headers[0])}</th>
        <th>{t(headers[1])}</th>
        <th>{t(headers[2])}</th>
        <th>{t(headers[3])}</th>
      </tr>
    </thead>
  );
};

class UserRow extends Component {
  constructor(props) {
    super(props);
    this.button = null;
    this.abortController = new AbortController();
    this.state = {
      fetchedUser: false,
      user: null,
    };
    this.setButtonRef = this.setButtonRef.bind(this);
    this.removeUser = this.removeUser.bind(this);
    this.onRemove = this.onRemove.bind(this);
  }

  async componentDidMount() {
    const { userId } = this.props;
    const {
      userInfo: { token },
      selectedLab: labId,
      showError,
    } = this.context;
    try {
      const user = await getLabUser({ token, userId, labId });
      this.setState({
        fetchedUser: true,
        user
      }); 
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(translateForUser(error));
    }
  };

  componentWillUnmount(){
    this.abortController.abort();
  }

  onRemove() {
    const { openModal, t } = this.context;
    openModal({
      body: t('lab.dashboard.do-you-confirm-remove-user'),
      confirmText: t('lab.dashboard.confirm-remove-user'),
      cancelText: t('lab.dashboard.cancel-remove-user'),
      onConfirm: this.removeUser,
      onCancel: doNothing,
    });
  }

  setButtonRef(element) {
    this.button = element;
  };

  async removeUser() {
    const { wait = () => ({}), endWait = () => ({}) } = this.button || {};
    const { userId } = this.props;
    const {
      userInfo: { token },
      selectedLab: labId,
      getUserLabs,
      showSuccess,
      showError,
      t,
    } = this.context;
    try {
      wait();
      await removeLabUser({ token, userId, labId }, this.abortController.signal);
      showSuccess(t('lab.dashboard.user-remove-success'));
      await getUserLabs();
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  };

  render() {
    const { fetchedUser, user = {} } = this.state;
    const { rowNum } = this.props;
    const { firstName = '', lastName = '', mobileNumber = '' } = user || {};
    const { i18n } = this.context;
    const { language = 'fa' } = i18n;


    if (!fetchedUser) return <TableSpinner />;

    return (
      <tr>
        <td>
          <RemoveUserButton
            onClick={this.onRemove}
            innerRef={this.setButtonRef}
          />
        </td>
        <th scope="row">{polishNumber(`${rowNum + 1}`, language)}</th>
        <td>{firstName}</td>
        <td>{lastName}</td>
        <td className="ltr">{polishNumber(mobileNumber, language)}</td>
      </tr>
    );
  }
}

UserRow.contextType = AppContext;

const UserRows = ({ users = [] }) => (
  <tbody>
    {users.map((userId, index) => (
      <UserRow key={userId} userId={userId} rowNum={index} />
    ))}
  </tbody>
);

const LabPersonnelTable = ({ users = [] }) =>
  users.length > 0 && (
    <Table responsive striped hover size="sm" className="lab-personnel-table">
      <TableHeader headers={tableHeaders} />
      <UserRows users={users || []} />
    </Table>
  );

export default LabPersonnelTable;

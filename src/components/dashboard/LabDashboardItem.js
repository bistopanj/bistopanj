import React from 'react';
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom'
import ContentCard from '../ContentCard';
import './LabDashboardItem.css';

const LabDashboardItem = ({ text = '', title = '', to = '', logo = null }) => (
  <Col xs="12" s="6" md="5" lg="3" className="card-container">
    <Link to={to}>
      <ContentCard
        logo={logo}
        logoColor="#990099"
        title={title}
        body={text}
        className="dashboard-content-card"
      />
    </Link>
  </Col>
);

export default LabDashboardItem;
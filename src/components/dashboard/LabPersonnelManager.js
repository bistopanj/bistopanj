import React from 'react';
import LabPersonnelTable from './LabPersonnelTable';
import LabPersonnelAdder from './LabPersonnelAdder';

const LabPersonnelManager = ({ lab = {} }) => (
  <>
    <LabPersonnelTable users={lab.users} />
    <LabPersonnelAdder users={lab.users} />
  </>
);

export default LabPersonnelManager;
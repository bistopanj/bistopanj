import React, { Component } from 'react';
import { Form } from 'reactstrap';
import './LabPersonnelAdder.css';
import { parseMobileNumber } from '../../utils/tools';
import AsyncButton from '../AsyncButton';
import { addUserToLab } from '../../services/api.service';
import FormItem from './FormItem';
import { AppContext } from '../../contexts';
import { translateForUser, errorTypes } from '../../errors';

const maxWidth = 576;
const maxUsers = 5;
const formStyle = { width: '100%', maxWidth };

const doNothing = () => ({});

class LabPersonnelAdder extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.mobileNumber = null;
    this.firstName = null;
    this.lastName = null;
    this.button = null;
    this.state = {
      readyToSubmit: false,
      mobileNumber: null,
    };
  }

  componentWillUnmount(){
    this.abortController.abort();
  }

  handleMobileNumberChange = e => {
    const text = e.target.value;
    const mobileNumber = parseMobileNumber(text);
    if(mobileNumber){
      this.setState({
        readyToSubmit: true,
        mobileNumber: mobileNumber.standardFormat,
      });
    }
    else {
      this.setState({
        readyToSubmit: false,
        mobileNumber: null,
      });
    }
  };

  setMobileNumberRef = element => {
    this.mobileNumber = element;
  };

  setFirstNameRef = element => {
    this.firstName = element;
  };

  setLastNameRef = element => {
    this.lastName = element;
  };

  setButtonRef = element => {
    this.button = element;
  };

  clearForm = () => {
    this.firstName.value = '';
    this.lastName.value = '';
    this.mobileNumber.value = '';
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const {
      userInfo: { token },
      selectedLab: labId,
      getUserLabs,
      showSuccess,
      showError,
      t,
    } = this.context;
    const { mobileNumber, readyToSubmit } = this.state;
    if(!readyToSubmit)
      return false;
    const { wait = doNothing, endWait = doNothing } = this.button || {};

    wait();
    const firstName = this.firstName.value;
    const lastName = this.lastName.value;
    const newUser = { mobileNumber };
    if(firstName)
      newUser.firstName = firstName;
    if(lastName)
      newUser.lastName = lastName;

    try{
      await addUserToLab({
        token,
        user: newUser,
        labId,
      }, this.abortController.signal);
      showSuccess(t('lab.dashboard.user-added-success'));
      this.clearForm();
      await getUserLabs();
      return true;
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
      return false;
    } finally {
      endWait();
    }
  };

  render() {
    const { readyToSubmit } = this.state;
    const { users = [] } = this.props;
    const { t } = this.context;

    if(users.length >= maxUsers)
      return (
        <p>{t('lab.dashboard.lab-is-full')}</p>
      );
    return (
      <Form
        className="lab-personnel-adder"
        style={formStyle}
        onSubmit={this.onSubmit}
      >
        <fieldset className="border p-2">
          <legend className="w-auto">{t('lab.dashboard.add-personnel-section')}</legend>
          <FormItem
            name="mobileNumber"
            label={t('lab.dashboard.mobile-num-input')}
            type="tel"
            id="lab-new-user-mobile-number"
            placeholder={t('lab.login.mobile-num-placeholder')}
            required
            onChange={this.handleMobileNumberChange}
            innerRef={this.setMobileNumberRef}
          />
          <FormItem
            name="firstName"
            label={t('general.name')}
            type="text"
            id="lab-new-user-first-name"
            innerRef={this.setFirstNameRef}
          />
          <FormItem
            name="lastName"
            label={t('general.surname')}
            type="text"
            id="lab-new-user-last-name"
            innerRef={this.setLastNameRef}
          />
          <p className="subText" style={{fontSize: '0.6rem'}}>{t('lab.dashboard.required-fields-desc')}</p>
          <AsyncButton block size="lg" color="info" disabled={!readyToSubmit} ref={this.setButtonRef}>
            {t('lab.dashboard.add-personnel')}
          </AsyncButton>
        </fieldset>
      </Form>
    );
  }
}

LabPersonnelAdder.contextType = AppContext;

export default LabPersonnelAdder;

import React, { Component } from 'react';
import { Form } from 'reactstrap';
import './LabInfoForm.css';
import FormItem from './FormItem';
import { editLabInfo } from '../../services/api.service';
import { AppContext } from '../../contexts';
import AsyncButton from '../AsyncButton';
import { translateForUser, errorTypes } from '../../errors';

const maxWidth = 576;
const formStyle = { width: '100%', maxWidth };

const doNothing = () => ({});

class LabInfoForm extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.name = null;
    this.description = null;
    this.confirmButton = null;
    this.cancel = this.cancel.bind(this);
    this.state = { readyToSubmit: true };
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  setNameRef = element => {
    this.name = element;
  };

  setDescriptionRef = element => {
    this.description = element;
  };

  handleNameChange = e => {
    const newName = e.target.value || '';
    this.setState({ readyToSubmit: newName.length > 0 });
  };

  setConfirmButtonRef = element => {
    this.confirmButton = element;
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const { readyToSubmit } = this.state;
    if (!readyToSubmit) return false;
    const {
      userInfo: { token },
      selectedLab: labId,
      getUserLabs,
      showError,
    } = this.context;
    const { wait = doNothing , endWait = doNothing } = this.confirmButton || {};
    const { onFinish= () => ({}) } = this.props;
    const name = this.name.value;
    const description = this.description.value;
    wait();
    try {
      await editLabInfo({ token, labId, data: { name, description }}, this.abortController.signal);
      await getUserLabs();
      onFinish();
      return true;
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(translateForUser(error));
      return false;
    } finally {
      endWait();
    }
  };

  cancel() {
    const { onFinish = doNothing } = this.props;
    onFinish();
  }

  render() {
    const { lab = {} } = this.props;
    const { name, description } = lab;
    const { readyToSubmit } = this.state;
    const { t } = this.context;

    return (
      <Form
        className="lab-info-form"
        style={formStyle}
        onSubmit={this.onSubmit}
      >
        <fieldset className="border p-2">
          <legend className="w-auto">{t('lab.dashboard.lab-info')}</legend>
          <FormItem
            name="name"
            label={t('lab.dashboard.lab-name')}
            type="text"
            id="lab-info-form-name"
            required
            defaultValue={name}
            onChange={this.handleNameChange}
            innerRef={this.setNameRef}
          />
          <p className="subText">{t('lab.dashboard.this-name-is-used-at')}</p>
          <FormItem
            name="description"
            label={t('lab.dashboard.lab-desc')}
            type="textarea"
            id="lab-info-form-description"
            defaultValue={description}
            innerRef={this.setDescriptionRef}
          />
          <p className="subText">{t('lab.dashboard.desc-is-used-at')}</p>

          <AsyncButton block outline size="lg" color="info" disabled={!readyToSubmit} ref={this.setConfirmButtonRef}>
            {t('lab.dashboard.save-changes')}
          </AsyncButton>
          <AsyncButton block outline size="lg" color="secondary" onClick={this.cancel} >
            {t('lab.dashboard.cancel-changes')}
          </AsyncButton>
        </fieldset>
      </Form>
    );
  }
}

LabInfoForm.contextType = AppContext;

export default LabInfoForm;

import React from 'react';
import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import './LabResultsTable.css';
import { getLocalTime, getLocalDate, polishNumber, buildQueryString } from '../../utils/tools';
import { useAuth, useAppContext } from '../../contexts';

const tableHeaders = [
  'lab.dashboard.test-result',
  'lab.dashboard.customer-mobile-number',
  'lab.dashboard.sent-by',
  'lab.dashboard.date',
  'lab.dashboard.time',
];

const buildSearchStrForResult = ({ lab = {}, result = {} }) => {
  const { name: labName, description: labDescription } = lab;
  const { photoUrl } = result;
  const query = { labName, labDescription, photoUrl };
  const queryStr = buildQueryString(query);
  return `?${queryStr}`;
};

const LabUser = ({ user = {} }) => {
  const { i18n } = useAppContext();
  const { language = 'fa' } = i18n;
  if(typeof user === 'string')
    return <p>{user}</p>;
  
  const { _id: id, firstName, lastName, mobileNumber } = user;
  if(firstName || lastName)
    return <p>{`${firstName || ''} ${lastName || ''}`}</p>
  
  if (mobileNumber)
    return <p className="text-center ltr">{polishNumber(mobileNumber, language)}</p>
  
  return <p>{id}</p>
};

const TableHeader = ({ headers }) => {
  const { t } = useAppContext();
  return (
    <thead className="thead-dark">
      <tr>
        {headers.map(header => (
          <th key={header}>{t(header)}</th>
        ))}
      </tr>
    </thead>
  );
};

const TableRows = ({ results = [], lab = {} }) => (
  <tbody>
    {results.map((result, index) => (
      <ResultRow key={result._id || index} result={result} lab={lab}/>
    ))}
  </tbody>
);

const ResultRow = ({ result = {}, lab = {} }) => {
  const { t, i18n } = useAppContext();
  const { language = 'fa' } = i18n;
  return (
    <tr>
      <td>
        <Link
          to={{
            pathname: `/lab/result/${result._id}`,
            search: buildSearchStrForResult({ lab, result }),
          }}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('lab.dashboard.visit')}
        </Link>
      </td>
      <td className="ltr">{polishNumber(result.clientMobileNumber, language)}</td>
      <td>
        <LabUser user={result.createdBy}/>
      </td>
      <td>{getLocalDate(result.createdAt, language)}</td>
      <td>{getLocalTime(result.createdAt, language)}</td>
    </tr>
  );
};

const LabResultsTable = ({ results = [] }) => {
  const { selectedLab, labs } = useAuth();
  const theLab = labs.find(lab => lab._id === selectedLab);
  
  if(!Array.isArray(results)) return null;
  if(results.length === 0)
    return null;
  
  return (
    <Table responsive striped hover size="sm" className="lab-result-table">
      <TableHeader headers={tableHeaders} />
      <TableRows results={results} lab={theLab}/>
    </Table>
  );
};

export default LabResultsTable;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Spinner } from 'reactstrap';
import './LabResultViewer.css';
import { getLabResults } from '../../services/api.service';
import { AppContext, useAppContext } from '../../contexts';
import LabResultsTable from './LabResultsTable';
import Logo from '../Logo';
import AsyncButton from '../AsyncButton';
import { translateForUser, errorTypes } from '../../errors';

const numberPerLoad = 20;

const NoResultPlaceholder = () => {
  const { t } = useAppContext();
  return (
    <p>
      {t('lab.dashboard.no-result-sent')}
      <br />
      {t('lab.dashboard.use-mobile-app')}
      <br />
      <Link to="/lab/dashboard/apps">
      {t('lab.dashboard.download-app')}
      </Link>
    </p>
  );
};

const doNothing = () => ({});

class LabResultsViewer extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.loadMoreButton = null;
    this.refreshButton = null;
    this.state = {
      results: null,
      finished: true
    };
    this.setLoadMoreButtonRef = this.setLoadMoreButtonRef.bind(this);
    this.setRefreshButtonRef = this.setRefreshButtonRef.bind(this);
    this.refresh = this.refresh.bind(this);
    this.loadMoreResults = this.loadMoreResults.bind(this);
  }

  componentDidMount() {
    this.refresh();
  };

  componentWillUnmount() {
    this.abortController.abort();
  }

  setLoadMoreButtonRef(element) {
    this.loadMoreButton = element;
  };

  setRefreshButtonRef(element) {
    this.refreshButton = element;
  };

  async refresh() {
    const {
      userInfo: { token },
      selectedLab: labId,
      showError,
      t,
    } = this.context;
    const { wait = doNothing, endWait = doNothing } = this.refreshButton || {};
    const query = {};
    try {
      wait();
      const newResults = await getLabResults(
        { token, labId, query },
        this.abortController.signal
      );
      this.setState({
        results: newResults,
        finished: newResults.length < numberPerLoad
      });
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  };

  async loadMoreResults() {
    const {
      userInfo: { token },
      selectedLab: labId,
      showError,
      t,
    } = this.context;
    const { results: oldResults } = this.state;
    const { wait = doNothing, endWait = doNothing } = this.loadMoreButton || {};
    const query = {};
    if (oldResults.length > 0)
      query.createdBefore = oldResults[oldResults.length - 1].createdAt;
    wait();
    try {
      const newResults = await getLabResults({ token, labId, query });
      const allResults = oldResults.concat(newResults);
      this.setState({
        results: allResults,
        finished: newResults.length < numberPerLoad
      });
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
    } finally {
      endWait();
    }
  };

  render() {
    const { results, finished } = this.state;
    if (!results) return <Spinner size="sm" color="info" />
    const { t } = this.context;
    if (results.length === 0) return <NoResultPlaceholder />;

    return (
      <>
        <p>{t('lab.dashboard.here-are-your-results')}</p>
        <div className="lab-results-viewer">
          <AsyncButton
            className="transparent-button"
            aria-label="refresh-results"
            onClick={this.refresh}
            color="secondary"
            ref={this.setRefreshButtonRef}
          >
            <Logo name="sync" size={25} color="rgba(0,0,0,0.2)" />
          </AsyncButton>
          <LabResultsTable results={results} />
          {!finished && (
            <AsyncButton
              color="info"
              onClick={this.loadMoreResults}
              ref={this.setLoadMoreButtonRef}
            >
              {t('lab.dashboard.load-more')}
            </AsyncButton>
          )}
        </div>
      </>
    );
  }
}

LabResultsViewer.contextType = AppContext;

export default LabResultsViewer;

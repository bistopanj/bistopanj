import React from 'react';
import { Button } from 'reactstrap';
import { useAppContext } from '../../contexts';
import Logo from '../Logo';
import { polishNumber } from '../../utils/tools';

const containerStyle = {
  display: 'flex',
  flexDirection: 'row',
  padding: 0,
  marginTop: '1rem'
};

const textStyle = {
  fontSize: '0.7rem',
  fontWeight: 'normal',
  padding: 0
};

const calculateRemainings = lab => {
  const { credit = 0, trialCredit = 0, resultsSent = 0 } = lab;
  if (resultsSent <= trialCredit) return [trialCredit - resultsSent, credit];

  const remainingCredit = credit + trialCredit - resultsSent;
  if (remainingCredit > 0) return [0, remainingCredit];

  return [0, 0];
};

const LabCredit = ({ lab = {} }) => {
  const { t, i18n, openModal } = useAppContext();
  const [remainingTrial, remainingCredit] = calculateRemainings(lab);

  const text = `${t('lab.dashboard.remaining-credit')} ${polishNumber(
    `${remainingTrial}`,
    i18n.language
  )} ${t('lab.dashboard.trial')} + ${polishNumber(
    `${remainingCredit}`,
    i18n.language
  )} `;
  const showInstructions = () => {
    openModal({
      title: t('lab.dashboard.add-credit'),
      body: t('lab.dashboard.add-credit-desc'),
      confirmText: t('general.confirm'),
      noCancel: true
    });
  };

  return (
    <div style={containerStyle}>
      <Button color="link" onClick={showInstructions} className="p-0 m-0">
        <Logo name="plus-square" color="#28a745" />
      </Button>
      <div className="m-0 p-0 d-flex justify-content-center align-items-center">
        <p className="my-0 mx-2" style={textStyle}>
          {text}
        </p>
      </div>
    </div>
  );
};

export default LabCredit;

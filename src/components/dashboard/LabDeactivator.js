import React, { Component } from 'react';
import { Collapse, Button, Card, CardBody, CardText } from 'reactstrap';
import './LabDeactivator.css';
import { AppContext, useAppContext } from '../../contexts';
import { deactivateLab } from '../../services/api.service';
import { translateForUser, errorTypes } from '../../errors';

const FinalDeactivateConfirm = ({ onConfirm = () => ({})}) => {
  const { t } = useAppContext();
  return (
    <Card>
      <CardBody>
        <CardText>
          {t('lab.dashboard.deactivation-disclaimer')}
        </CardText>
      </CardBody>
      <Button size="lg" color="danger" onClick={onConfirm}>
        {t('lab.dashboard.final-confirm-deactivation')}
      </Button>
    </Card>
  );
};

class LabDeactivator extends Component {
  constructor(props) {
    super(props);
    this.abortController = new AbortController();
    this.state = { collapse: false };
  }

  componentWillUnmount(){
    this.abortController.abort();
  }

  toggleCollapse = () => {
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  };

  deactivate = async () => {
    const {
      userInfo: { token },
      selectedLab: labId,
      logout,
      showError,
      showSuccess,
      t,
    } = this.context;

    try {
      await deactivateLab({ token, labId }, this.abortController.signal);
      logout();
      showSuccess(t('lab.dashboard.deactivation-success'), { duration: 15000 });
      return true;
    } catch (error) {
      if (error.name !== errorTypes.AbortError)
        showError(t(translateForUser(error)));
      return false;
    }
  };

  render() {
    const { collapse } = this.state;
    const { t } = this.context;
    return (
      <div className="lab-deactivator-container">
        <Button color="info" outline size="lg" onClick={this.toggleCollapse}>
          {!collapse ? t('lab.dashboard.deactivate') : t('lab.dashboard.close') }
        </Button>
        <Collapse isOpen={collapse} >
          <FinalDeactivateConfirm onConfirm={this.deactivate} />
        </Collapse>
      </div>
    );
  }
}

LabDeactivator.contextType = AppContext;

export default LabDeactivator;

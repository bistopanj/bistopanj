import React from 'react';
import { Row } from 'reactstrap';
import LabDashboardItem from './LabDashboardItem';


const LabPanelDescription = ({ items = [] }) => (
  <Row>
    {items.map((item, index) => (
      <LabDashboardItem
        logo={item.logo}
        text={item.text}
        title={item.title}
        to={item.to}
        key={item.key || index}
      />
    ))}
  </Row>
);

export default LabPanelDescription;
import React from 'react';
import { Container } from 'reactstrap';
import '../App.css';
import ImageJumbo from './ImageJumbo';
import background from '../assets/lab_customer.jpg';

const valueText = 'نتیجه‌ی آزمایش‌های‌تان را الکترونیکی بفرستید.';
const actionButtonText = 'راه‌اندازی';

const LabValueJumbo = props => (
  <div className="p-0 m-0 bg-dark">
    <Container className="p-0">
      <ImageJumbo
        className="d-flex text-center justify-content-center align-items-end my-0"
        fluid
        background={background}
      >
        <div className="bg-dark rounded text-white px-3 py-2 mx-2 opacity-70">
          <p>{valueText}</p>
          <a className="light" href="/">
            {actionButtonText}
          </a>
        </div>
      </ImageJumbo>
    </Container>
  </div>
);

export default LabValueJumbo;

import React from 'react';
import { Nav, NavItem, Button } from 'reactstrap';
import { NavLink, withRouter } from 'react-router-dom';
import './SidebarMenu.css';
import Logo from '../Logo';
import { useAppContext } from '../../contexts';

const doNothing = () => ({});

const decideRerender = (prevProps, nextProps) => (prevProps.isOpen === nextProps.isOpen);

const SidebarMenu = React.memo(({ isOpen = true, close, items = [], dir = 'right' }) => {
    const sidebarBaseClass = `sidebar-${dir}`;
    const { t } = useAppContext();
    const sidebarClass = `${sidebarBaseClass} ${isOpen ? 'slideIn' : 'slideOut'}`;
    const backArrowName = `arrow-${dir}`;
  return (
    <>
      <div id="sidebar-menu" className={sidebarClass}>
        <div className="sidebar-header">
          <Button onClick={close} close>
            <Logo name={backArrowName} size={20} color="rgba(0,0,0,0.5)" />
          </Button>
        </div>
        <Nav vertical className="list-unstyled components">
            {items.map(item => (
              <NavItem key={`sidebar-item-${item.key}`}>
                <NavLink
                  exact={item.to === '/'}
                  to={item.to}
                  className="nav-link"
                  onClick={close}
                >
                  {t(item.title)}
                </NavLink>
              </NavItem>
            ))}
          </Nav>
      </div>
      <div
        id="sidebar-menu-overlay"
        onClick={close}
        role="button"
        tabIndex="0"
        aria-label="Close Sidebar"
        onKeyPress={doNothing}
        className={isOpen ? 'show' : 'hide'}
      />
    </>
  );
}, decideRerender);

export default withRouter(SidebarMenu);
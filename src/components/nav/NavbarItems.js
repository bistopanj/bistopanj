import React from 'react';
import { Nav, NavItem } from 'reactstrap';
import { NavLink, withRouter } from 'react-router-dom';
import { useAppContext } from '../../contexts';

const NavbarItems = ({
  items = [],
}) => {
  const { direction, t } = useAppContext();

  return (
    <Nav className={direction === 'rtl' ? 'mr-auto': 'ml-auto'} >
      {items.map(item => (
        <NavItem key={`navbar-item-${item.key}`}>
          <NavLink
            exact={item.to === '/'}
            className="nav-link"
            to={item.to}
          >
            {t(item.title)}
          </NavLink>
        </NavItem>
      ))}
    </Nav>
  );
};

export default withRouter(NavbarItems);
import React, { Component } from 'react';
import { Navbar, Container, Nav, NavbarToggler, Collapse } from 'reactstrap';
import { NavLink, withRouter, matchPath } from 'react-router-dom';
import './NavBar.css';
import logos, { bistopanj as bistopanjLogo } from '../../contents/logos';
import LangSelector from '../LangSelector';
import NavbarItems from './NavbarItems';
import SidebarMenu from './SidebarMenu';
import { useAppContext, AppContext } from '../../contexts';
import defaultMenuItems from '../../data/default-menu-items';

const shrinkAt = 200; // make the navbar smaller when the scroll reached this vertical position

const doNothing = () => ({});

const ContainerApplied = ({ container = true, children }) => {
  if (container) return <Container>{children}</Container>;
  return <>{children}</>;
};

const NavLogo = ({ logo = 'bistopanj', link = '/', small = false }) => {
  const { direction } = useAppContext();
  const selectedLogo = logo in logos ? logos[logo] : bistopanjLogo;
  const logoClass = `navbar-logo${small ? ' small-logo' : ''}`;
  const linkClassName = direction === 'ltr' ? 'm-1 mr-auto' : 'm-1 ml-auto';
  return (
    <NavLink className={linkClassName} to={link}>
      <img
        className={logoClass}
        src={selectedLogo.src}
        srcSet={selectedLogo.srcSet || null}
        alt={selectedLogo.alt}
      />
    </NavLink>
  );
};

const NavToggler = ({ onClick = doNothing }) => (
  <NavbarToggler onClick={onClick} className="custom-toggler border-0">
    <svg
      height="32"
      width="32"
      viewBox="0 0 32 32"
      className="navbar-toggler-icon"
    >
      <g stroke="currentColor">
        <path
          fill="#ffffff"
          strokeWidth="2"
          strokeLinecap="round"
          strokeMiterlimit="10"
          d="M4 8h24M4 16h24M4 24h24"
        />
      </g>
    </svg>
  </NavbarToggler>
);

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shrink: false,
      sidebarOpen: false
    };

    this.listenScrollEvent = this.listenScrollEvent.bind(this);
    this.toggleSideMenu = this.toggleSideMenu.bind(this);
    this.closeSideMenu = this.closeSideMenu.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.listenScrollEvent);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.listenScrollEvent);
  }

  toggleSideMenu() {
    this.setState(state => ({ sidebarOpen: !state.sidebarOpen }));
  }

  closeSideMenu() {
    this.setState({ sidebarOpen: false });
  }

  listenScrollEvent() {
    const { shrink } = this.state;
    if (!shrink && window.scrollY > shrinkAt) this.setState({ shrink: true });

    if (shrink && window.scrollY < shrinkAt) this.setState({ shrink: false });
  }

  render() {
    const {
      location = {},
      transparent = false,
      container = false,
      logoLink = '/',
      logo = 'bistopanj',
      menuItems = defaultMenuItems,
      color = null,
      exclude = null
    } = this.props;

    const { shrink, sidebarOpen } = this.state;
    const { direction } = this.context;

    if (exclude) {
      const mustExclude = matchPath(location.pathname, {
        path: exclude,
        exact: false,
        strict: false
      });
      if (mustExclude) return null;
    }

    const navbarClassName = `my-navbar${
      transparent && !shrink ? ' transparent' : ''
    }${shrink ? ' py-0 shrink' : ' py-2'}${color ? ` ${color}` : ''}`;

    return (
      <header>
        <Navbar fixed="top" expand="sm" className={navbarClassName}>
          <ContainerApplied container={container}>
            <NavLogo logo={logo} link={logoLink} small={shrink} />
            <Collapse isOpen={false} navbar>
              <NavbarItems items={menuItems} />
            </Collapse>
            <Nav>
              <LangSelector nav inNavbar />
            </Nav>
            <NavToggler onClick={this.toggleSideMenu} />
          </ContainerApplied>
          <SidebarMenu
            isOpen={sidebarOpen}
            close={this.closeSideMenu}
            items={menuItems}
            dir={direction==='rtl'?'left':'right'}
          />
        </Navbar>
      </header>
    );
  }
}

NavBar.contextType = AppContext;

export default withRouter(NavBar);

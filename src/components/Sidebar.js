import React from 'react';
import { Nav, NavItem } from 'reactstrap';
import { NavLink, withRouter, Link } from 'react-router-dom';
import './Sidebar.css';
import { useAppContext } from '../contexts';
import LangSelector from './LangSelector';

const SidebarHeader = ({ logo = null, text = '', subtextComponent = null }) => (
  <div className="side-bar-header">
    {logo && (
      <img
        className="side-bar-logo"
        src={logo.src}
        srcSet={logo.srcSet || null}
        alt={logo.alt || 'side-bar-logo'}
      />
    )}
    <p className="my-heading-font"> {text} </p>
    {subtextComponent}
  </div>
);

const SidebarItem = ({ item, onItemSelect = () => ({}) }) => {
  const { t } = useAppContext();
  return (
    <NavItem>
      <NavLink
        exact={item.to === '/lab/dashboard'}
        to={item.to}
        className="nav-link"
        onClick={onItemSelect}
      >
        {t(item.title)}
      </NavLink>
    </NavItem>
  );
};

const SidebarFooter = () => {
  const { t } = useAppContext();
  return (
    <footer className="footer text-center">
      <p style={{ fontSize: '0.6rem' }}>
        {t('lab.dashboard.created-in')}{' '}
        <Link to="/">{t('lab.dashboard.bistopanj')}</Link>
      </p>
    </footer>
  );
};

const Sidebar = ({
  onItemSelect = () => ({}),
  items = [],
  logo = null,
  headerText,
  headerSubtextComponent = null,
  lang = false
}) => (
  <div className="sidebar-container">
    <SidebarHeader
      logo={logo}
      text={headerText}
      subtextComponent={headerSubtextComponent}
    />
    <Nav vertical className="list-unstyled components text-center">
      {lang && (
        <>
          <LangSelector />
          <br />
          <br />
        </>
      )}
      {items.map((item, index) => (
        <SidebarItem
          key={item.id || index}
          item={item}
          onItemSelect={onItemSelect}
        />
      ))}
    </Nav>
    <SidebarFooter />
  </div>
);

export default withRouter(Sidebar);

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from '../contexts/auth';

const PrivateRoute = ({ component: Component, requires = ['token'], ...rest }) => {
  const { isAuthenticated, labs, selectedLab } = useAuth();
  return (
    <Route
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      render={props => {
        const state = {
          from: props.location,
          data: ((rest.location.state || {}).data || {})
        };
        if(requires.includes('token') && !isAuthenticated)
          return(
            <Redirect 
              to={{
                pathname: '/lab/login',
                state,
              }}
            />
          );

        if(requires.includes('labs') && !labs)
          return(
            <Redirect 
              to={{
                pathname: '/lab/update',
                state,
              }}
            />
          );

        if(requires.includes('selectedLab') && !selectedLab)
          return(
            <Redirect 
              to={{
                pathname: '/lab/choose',
                state,
              }}
            />
          );
        // eslint-disable-next-line react/jsx-props-no-spreading
        return (<Component {...props} />);
      }}
    />
  );
};

export default PrivateRoute;
import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import './JumboMultiCol.css';
import ContentCard from './ContentCard';
import MyGrid from './MyGrid';
import { useAppContext } from '../contexts';

const JumboMultiCol = props => {
  const { t } = useAppContext();
  const {
    id = null,
    content,
    logoColor = null,
    textColor = 'black',
    jumboTitleColor = 'black',
    itemTitleColor = 'black',
    backgroundColor = null,
    cols = 2
  } = props;

  const JumboContentCard = ps => (
    <ContentCard
      logoColor={logoColor}
      className="jumbo-content-card"
      titleColor={itemTitleColor}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...ps}
    />
  );

  const generateGridContent = (cnt, key) => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <JumboContentCard {...cnt} key={key} />
  );

  return (
    <Jumbotron
      id={id}
      fluid
      className="my-0 text-center"
      style={{ backgroundColor }}
    >
      <Container>
        <h3 className="my-heading-font" style={{ color: jumboTitleColor }}>
          {t(content.title)}
        </h3>
        <p style={{ color: textColor }}>{t(content.subtitle)}</p>
        <MyGrid
          style={{ color: textColor }}
          contents={content.items}
          cols={cols}
          genItem={generateGridContent}
          colClassName="p-3"
        />
      </Container>
    </Jumbotron>
  );
};

export default JumboMultiCol;

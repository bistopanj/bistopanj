import React from 'react';
import { Jumbotron, Button, Container, Row, Col } from 'reactstrap';

const Jumbo = props => (
  <Jumbotron fluid style={{ minHeight: 'calc(100vh - 56px)' }}>
    <Container>
      <Row>
        <Col md="6">
          <h1 className="display-3 font-weight-bold">لب</h1>
          <p className="lead">برای الکترونیکی کردن آزمایشگاه‌های پزشکی</p>
          <hr className="my-2" />
          <p>همین الآن سیستم ارسال الکترونیکی را راه‌اندازی کنید.</p>
          <p>چیزی بیش‌تر از گوشی هوشمند احتیاج نیست.</p>
          <p className="lead">
            <Button color="primary">راه‌اندازی برای آزمایشگاه شما</Button>
          </p>
        </Col>
        <Col md="6">Second Col</Col>
      </Row>
    </Container>
  </Jumbotron>
);

export default Jumbo;

/*
<Jumbotron fluid style={{minHeight: 'calc(100vh - 56px)'}}>
        <Container>
            <h1 className="display-3 font-weight-bold">لب</h1>
            <p className="lead">نتیجه‌ی آزمایش‌هایتان را خیلی راحت با پیامک برای بیمار بفرستید</p>
            <hr className="my-2" />
            <p> بیمار را برای دریافت نتیجه به آزمایشگاه نکشانید.</p>
            <p>همین الآن سیستم ارسال هوشمند را راه‌اندازی کنید.</p>
            <p>چیزی بیش‌تر از یک گوشی هوشمند احتیاج نیست.</p>
            <p className="lead">
                <Button color="primary">راه‌اندازی برای آزمایشگاه شما</Button>
            </p>
        </Container>
    </Jumbotron>
*/

import React from 'react';
import { Card, CardBody, CardText, CardTitle } from 'reactstrap';
import Logo from './Logo';
import { useAppContext } from '../contexts';

const ContentCard = ({
  logo = null,
  title = 'Card Title',
  body = 'Card Body',
  titleColor = 'black',
  style = {},
  className = '',
  logoColor = null,
  logoSize = '5rem'
}) => {
  const { t } = useAppContext();
  return (
    <Card
      className={`${className} text-center align-items-center border-0`}
      style={style}
    >
      {logo && <Logo name={logo} size={logoSize} color={logoColor} />}
      <CardBody className="mt-3" style={{ maxWidth: '75vw' }}>
        <CardTitle className="my-heading-font" style={{ color: titleColor }}>
          {t(title)}
        </CardTitle>
        <CardText>{t(body)}</CardText>
      </CardBody>
    </Card>
  );
};

export default ContentCard;

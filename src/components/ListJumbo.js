import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import '../App.css';

const listItemStyle = {
  backgroundColor: 'rgba(0,0,0,0.1)',
  padding: '15px 30px 15px 30px',
  margin: 15,
  fontSize: '1em',
  borderRadius: 10
};

const listStyle = {
  listStyleType: 'none',
  padding: 0,
  margin: 0
};

const ListJumbo = props => (
  <Jumbotron
    fluid
    className="my-0 text-white-50"
    style={{ backgroundColor: props.bg || '#5F9EA0' }}
  >
    <Container>
      <p className="h5 jumbo-header-text">{props.title}</p>
      <ul style={listStyle}>
        {props.items.map((listItem, i) => (
          <li key={i} className="text-right text-white" style={listItemStyle}>
            {listItem}
          </li>
        ))}
      </ul>
    </Container>
  </Jumbotron>
);

export default ListJumbo;
